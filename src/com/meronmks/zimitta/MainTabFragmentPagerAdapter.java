package com.meronmks.zimitta;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

public class MainTabFragmentPagerAdapter extends FragmentStatePagerAdapter {

	public MainTabFragmentPagerAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int position) {
		switch(position){
			case 0:
				return new TimeLineFragment();
			case 1:
				return new MentionTimeLineFragment();
			case 2:
				return new UserListFragment();
			case 3:
				return new DirectMessageTLFragment();
			default:
				return new Tab5Fragment();
		}
	}

	@Override
	public int getCount() {
		return 5;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		switch(position){
		case 0:
			return "Home";
		case 1:
			return "Mention";
		case 2:
			return "List";
		case 3:
			return "DM";
		default:
			return "Search(工事中という名の未実装)";
		}
	}
}
