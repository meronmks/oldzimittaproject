package com.meronmks.zimitta;

/**
 * Created by p-user on 2015/01/28.
 */

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class TweetDataBase extends SQLiteOpenHelper {

    public TweetDataBase(Context c) {
        super(c, "TweetDB.db", null, 1);
    }
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE TweetTable(" +
                    "tweet," +
                    "createdAt," +
                    "tweetID," +
                    "tweetDel" +
                    ");");
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //db.execSQL(DROP_TABLE);
        //onCreate(db);
    }
}
