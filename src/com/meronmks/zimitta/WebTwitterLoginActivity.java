package com.meronmks.zimitta;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import com.actionbarsherlock.app.SherlockActivity;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class WebTwitterLoginActivity extends SherlockActivity implements OnClickListener {

	private EditText mailText;
	private EditText passText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.webloginlayout);
        setTitle("Web版ログイン");

        //ボタンのクリックリスナー設定
        Button login = (Button)findViewById(R.id.button1);
        login.setOnClickListener(WebTwitterLoginActivity.this);

        mailText = (EditText)findViewById(R.id.editText1);
		passText = (EditText)findViewById(R.id.editText2);

	}

	@Override
	public void onClick(View v) {
		// TODO 自動生成されたメソッド・スタブ
		switch(v.getId()){
        case R.id.button1:
        	Intent SettingsApplications = new Intent(WebTwitterLoginActivity.this, SettingsApplicationsActivity.class);
        	SettingsApplications.putExtra("meil", mailText.getText().toString());
        	SettingsApplications.putExtra("pass", passText.getText().toString());
    		startActivity(SettingsApplications);
        	break;
		}
	}

}
