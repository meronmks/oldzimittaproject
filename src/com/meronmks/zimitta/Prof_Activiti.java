package com.meronmks.zimitta;

import com.actionbarsherlock.app.SherlockActivity;
import twitter4j.IDs;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.User;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.PaintDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.image.SmartImageView;

public class Prof_Activiti extends SherlockActivity implements OnClickListener{

	private Twitter mTwitter;
	private TextView ScreenName;
	private TextView UserName;
	private TextView ProfText;
	private TextView Location;
	private TextView URLText;
	private TextView UserStaticID;
	private Button Tweet;
	private Button Fav;
	private Button Following;
	private Button Follwers;
	private Button Follow;
	private Button Menu;
	private SmartImageView icon;
	private ProgressDialog progressDialog;
	private User user;
	private long Userid;
	private  SharedPreferences accountIDCount;
	private UserActionClass userAction;

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@SuppressLint("NewApi")
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_prof);

		//ﾊﾞｰからアイコンを消す
		getSupportActionBar().setDisplayShowHomeEnabled(false);
        PaintDrawable paintDrawable = new PaintDrawable(Color.argb(255,0,0,0));
        getWindow().setBackgroundDrawable(paintDrawable);
        ShowprogressDialog();
        ScreenName = (TextView)this.findViewById(R.id.NameView);
        UserName = (TextView)this.findViewById(R.id.ScreenNameView);
        ProfText = (TextView)this.findViewById(R.id.ProfTextView);
        icon = (SmartImageView) this.findViewById(R.id.icon);
        Location = (TextView)this.findViewById(R.id.LocView);
        URLText = (TextView)this.findViewById(R.id.URLTextView);
        Tweet = (Button)this.findViewById(R.id.TweetCountView);
        Fav = (Button)this.findViewById(R.id.FavCountView);
        Following = (Button)this.findViewById(R.id.FollowingCountView);
        Follwers = (Button)this.findViewById(R.id.FollwersCountView);
		Follow = (Button)this.findViewById(R.id.followButton);
		Menu = (Button)this.findViewById(R.id.MenuButton);
		UserStaticID = (TextView)this.findViewById(R.id.UserStaticID);
     	//ボタンのクリックリスナの作成
		Tweet.setOnClickListener(this);
		Fav.setOnClickListener(this);
		Following.setOnClickListener(this);
		Follwers.setOnClickListener(this);
		Follow.setOnClickListener(this);
		Menu.setOnClickListener(this);

        accountIDCount = getSharedPreferences("accountidcount", 0);
        if (!TwitterUtils.hasAccessToken(this,accountIDCount.getLong("ID_Num_Now", 0))) {
            Intent intent = new Intent(this, TwitterOAuthActivity.class);
            startActivity(intent);
            finish();
        }else {
            mTwitter = TwitterUtils.getTwitterInstance(this,accountIDCount.getLong("ID_Num_Now", 0));
			userAction = new UserActionClass(this,mTwitter);
            Intent intent = getIntent();
            Userid = intent.getLongExtra("UserID", BIND_ABOVE_CLIENT);
            Show_Prof();
        }

    }

	//ボタンクリック処理
		public void onClick(View v){
		      switch(v.getId()){
		        case R.id.TweetCountView:
		        	Intent TweetCountView = new Intent(Prof_Activiti.this, UserTimeLineActivity.class);
		        	TweetCountView.putExtra("UserID_TL", Userid);
		        	TweetCountView.putExtra("ScreenName", ScreenName.getText());
	        		startActivity(TweetCountView);
		            break;
		        case R.id.FavCountView:
		        	Intent FavCountView = new Intent(Prof_Activiti.this, UserFavActivity.class);
		        	FavCountView.putExtra("UserID_Fav", Userid);
		        	FavCountView.putExtra("ScreenName", ScreenName.getText());
	        		startActivity(FavCountView);
		        	break;
		        case R.id.FollowingCountView:
		        	Intent FollowingCountView = new Intent(Prof_Activiti.this, UserFollowActivity.class);
		        	FollowingCountView.putExtra("UserID_TL", Userid);
		        	FollowingCountView.putExtra("ScreenName", ScreenName.getText());
	        		startActivity(FollowingCountView);
		            break;
		        case R.id.FollwersCountView:
		        	Intent FollwersCountView = new Intent(Prof_Activiti.this, UserFollowersActivity.class);
		        	FollwersCountView.putExtra("UserID_TL", Userid);
		        	FollwersCountView.putExtra("ScreenName", ScreenName.getText());
	        		startActivity(FollwersCountView);
		        	break;
				  case R.id.followButton:
					  if(Follow.getText().equals("フォロー")) {
						  userAction.userFollow(Userid);
					  }else if(Follow.getText().equals("フォロー済み")){
						  userAction.userUnduFollow(Userid);
					  }else if(Follow.getText().equals("ブロック中")){

					  }
					  break;
				  case R.id.MenuButton:
					  List_Menu list = new List_Menu(this);
					  list.Prof_Menu(mTwitter,user);
					  break;
		      }
		}

		//ユーザー詳細取得
		private void Show_Prof() {
	    AsyncTask<Void, Void, User> task = new AsyncTask<Void, Void, User>() {
	    	@Override
	        protected User doInBackground(Void... params) {
	        	try {
	                return mTwitter.showUser(Userid);
	            } catch (TwitterException e) {
	                e.printStackTrace();
	        	}
	            return null;
	        }

		        @Override
		        protected void onPostExecute(User result) {
		        	progressDialog.dismiss();
		        	if(result != null){
						ScreenName.setText("@" + result.getScreenName());
						UserName.setText(result.getName());
						String str = result.getDescription();
						StringBuffer sb = new StringBuffer();
						char[] chr = str.toCharArray();
						for(int p=0;p<chr.length;p++) {
							if( chr[p] == ' ' || chr[p] == '　' ) continue;
							sb.append(chr[p]);
						}
						ProfText.setText(sb.toString());
						icon.setImageUrl(result.getProfileImageURL());
						Location.setText(result.getLocation());
						URLText.setText(result.getURLEntity().getDisplayURL());
						Tweet.setText("ツイート\r\n" + result.getStatusesCount());
						Fav.setText("お気に入り\r\n" + result.getFavouritesCount());
						Following.setText("フォロー\r\n" + result.getFriendsCount());
						Follwers.setText("フォロワー\r\n" + result.getFollowersCount());
						UserStaticID.setText("固有ID：" + result.getId());
						userAction.chackFollow(result, Follow);
						user = result;
		        	}else{
		        		Toast.makeText(Prof_Activiti.this, "取得失敗・・・", Toast.LENGTH_SHORT).show();
		        	}
		        }
		    };
		    task.execute();
		}

		//通信中とかのダイアログをだす
		private void ShowprogressDialog() {
			progressDialog = new ProgressDialog(Prof_Activiti.this);
	        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
	        progressDialog.setCancelable(true);
	        progressDialog.setMessage("読み込み中・・・");
	        progressDialog.setCancelable(false);
	        progressDialog.show();
		}
}
