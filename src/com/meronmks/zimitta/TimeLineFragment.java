package com.meronmks.zimitta;

import android.app.NotificationManager;
import android.content.*;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.actionbarsherlock.app.SherlockListFragment;
import twitter4j.*;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import twitter4j.json.DataObjectFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class TimeLineFragment extends SherlockListFragment {

	private TweetAdapter mAdapter;
	private Twitter mTwitter;
	private ShowRateLimit limit;
	private SharedPreferences sp,accountIDCount;
	private long OldStatus;
    private long NewStatus;
    private Boolean NewReloadFulg = true;
    private Context Activity;
    private ListView lv = null;
    private Status Tweet;
    //ミュートリスト保存用配列
    private MyUserStreamAdapter mMyUserStreamAdapter;
    private TwitterStream twitterStream;
    //ストリーミングで得たツイートのスタック用
    private ArrayList<Status> Streeming_stok;
    private Boolean Streeming_stok_Flug = false;
    private Status TempTweet = null;
	//ネットワーク接続判定用変数
	private boolean connected = false;
	private BroadcastReceiver connectivityActionReceiver;
	//DB
	static private SQLiteDatabase mydb;
	private TweetDataBase tdb;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO 自動生成されたメソッド・スタブ
		super.onActivityCreated(savedInstanceState);

		//Fragmentではこのメゾット以外でこれを実行すると落ちる
		lv = getListView();

		mAdapter = new TweetAdapter(Activity);
		setListAdapter(mAdapter);

		tdb = new TweetDataBase(Activity);
		mydb = tdb.getWritableDatabase();
		if(MainActivity.TLStatusIDs != null) {
			if (MainActivity.TLStatusIDs.size() == 0) {
				Get_My_ID();
				Get_My_Mute_List();
				reloadTimeLine();
			}
		}
	}

 	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Activity = getActivity();

        mMyUserStreamAdapter = new MyUserStreamAdapter();

		sp = PreferenceManager.getDefaultSharedPreferences(Activity);
        accountIDCount = Activity.getSharedPreferences("accountidcount", 0);
        SharedPreferences spOauth = Activity.getSharedPreferences(MainActivity.PREF_NAME + accountIDCount.getLong("ID_Num_Now", 0), Context.MODE_PRIVATE);

		//一般設定
		ConfigurationBuilder builder = new ConfigurationBuilder();
		{
			// Twitter4Jに対してOAuth情報を設定
			// アプリ固有の情報
			builder.setOAuthConsumerKey(getString(R.string.twitter_consumer_key));
			builder.setOAuthConsumerSecret(getString(R.string.twitter_consumer_secret));
			// アプリ＋ユーザー固有の情報
			builder.setOAuthAccessToken(spOauth.getString(MainActivity.TOKEN, null));
			builder.setOAuthAccessTokenSecret(spOauth.getString(MainActivity.TOKEN_SECRET, null));

			//HTTPタイムアウト設定(ミリ秒)
			builder.setHttpConnectionTimeout(10000);

			//StatusからJSON作成可にする
			builder.setJSONStoreEnabled(true);

			//ストリーミング時にリプも表示するかどうか？
			if(sp.getBoolean("Streeming_FF_Mention", false)) {
				builder.setUserStreamRepliesAllEnabled(true);
			}else{
				builder.setUserStreamRepliesAllEnabled(false);
			}
		}
        // 1. TwitterStreamFactory をインスタンス化する
        Configuration conf = builder.build();
        TwitterStreamFactory twitterStreamFactory = new TwitterStreamFactory(conf);
        // 2. TwitterStream をインスタンス化する
        twitterStream = twitterStreamFactory.getInstance();

        mTwitter = TwitterUtils.getTwitterInstance(Activity,accountIDCount.getLong("ID_Num_Now", 0));
    	limit = new ShowRateLimit(Activity);
        Streeming_stok = new ArrayList<Status>();
	}



	//タイムラインの非同期処理
		private void reloadTimeLine() {
			MainActivity.progresRun();
		AsyncTask<Void, Void, List<Status>> task = new AsyncTask<Void, Void, List<Status>>() {
	    	String exception;
	    	int position,y;
	    	@Override
	        protected List<twitter4j.Status> doInBackground(Void... params) {
	    		int i = Integer.parseInt(sp.getString("Load_Tweet", "20"));
	        	if ((NewStatus == 0) && (NewReloadFulg)){
	        	try {
	            	Paging p = new Paging();
	            	p.count(i);
	                return mTwitter.getHomeTimeline(p);
				} catch (TwitterException e){
					e.printStackTrace();
					exception = e.getMessage();
				} catch(Exception e)
				{
					e.printStackTrace();
					exception = e.getMessage();
				}
	        	}else if((NewStatus != 0) && (NewReloadFulg)){
	        		try {
	                	Paging p = new Paging();
	                	p.setSinceId(NewStatus);
	                	p.count(200);
	                    return mTwitter.getHomeTimeline(p);
	                } catch (TwitterException e) {
	                    e.printStackTrace();
	                    exception = e.getMessage();
	                }
	        	}else if(!NewReloadFulg){
	        		try {
	                	Paging p = new Paging();
	                	p.setMaxId(OldStatus);
	                	p.count(i);
	                    return mTwitter.getHomeTimeline(p);
					} catch (TwitterException e){
						e.printStackTrace();
						exception = e.getMessage();
					} catch(Exception e)
					{
						e.printStackTrace();
						exception = e.getMessage();
					}
	        	}
	            return null;
	        }

		        @Override
		        protected void onPostExecute(List<twitter4j.Status> result) {
		            if (result != null) {
		            	int count = 0;
		            	position = lv.getFirstVisiblePosition();

		            	try{
		            		y = lv.getChildAt(0).getTop();
		            	}catch(Exception e){
		            		y = 0;
		            	}
		            	Boolean mute = false;
		            	if(NewReloadFulg)
		            	{
			                for (twitter4j.Status status : result) {
			                	for(long ID : MainActivity.mutelist)
			                	{
			                		if((status.getUser().getId() == ID) && (sp.getBoolean("mute_flag", false)))
			                		{
			                			mute = true;
			                		}
			                	}
			                	if(!mute)
			                	{
									if(count == 0 &&  !sp.getBoolean("Streem_Flug", false)){
										MainActivity.TLStatusIDs.add(0,null);
										count++;
									}

				                    MainActivity.TLStatusIDs.add(count, status);
//									putDBTweet(status);
				                    count++;
			                	}
			                }
		            	}else{
		            		for (twitter4j.Status status : result) {
			                	for(long ID : MainActivity.mutelist)
			                	{
			                		if((status.getUser().getId() == ID) && (sp.getBoolean("mute_flag", false)))
			                		{
			                			mute = true;
			                		}
			                	}
			                	if(!mute)
			                	{
			            			if(count == 0)
				                    {
				                    	MainActivity.TLStatusIDs.remove(status);
				                    }
			            			count++;
				                    MainActivity.TLStatusIDs.add(status);
//									putDBTweet(status);
			                	}
		            		}
		            	}
		                if(count == 0){
							if(!sp.getBoolean("Streem_Flug", false)) {
								MainActivity.TLStatusIDs.add(0, null);
							}
		                	MainActivity.showToast("新着なし。");
		                }else{
							if(OldStatus == 0 || !NewReloadFulg) {
								MainActivity.TLStatusIDs.add(null);
							}
							mAdapter.clear();
							for (twitter4j.Status tweet : MainActivity.TLStatusIDs) {
								mAdapter.add(tweet);
							}
		                	MainActivity.showToast(count + "件更新完了！");
		                }
						if(NewStatus != 0 && NewReloadFulg){
							lv.setSelectionFromTop(position + count + 1, y);	//ListViewの表示位置をずらす
						}
						try {
							NewStatus = mAdapter.getItem(1).getId();
							OldStatus = mAdapter.getItem(mAdapter.getCount() - 2).getId();
						}catch (Exception e){
							e.printStackTrace();
						}
		            } else {
		            	MainActivity.showDialog("タイムラインの取得に失敗しました・・・\n" + exception);
		            }
		            limit.getHomeTimeLineLimit(mTwitter);
					MainActivity.progresStop();
		        }
		    };
		    task.execute();
		    //TL_load_lock = false;
		}

		//ミュートリスト取得
	    private void Get_My_Mute_List() {
		    AsyncTask<Void, Void, IDs> task = new AsyncTask<Void, Void, IDs>() {
		    	@Override
		        protected IDs doInBackground(Void... params) {
		        	try {
						return mTwitter.getMutesIDs(-1);
		            } catch (TwitterException e){
						e.printStackTrace();
					} catch(Exception e)
					{
						e.printStackTrace();
					}
		            return null;
		        }

			        @Override
			        protected void onPostExecute(IDs result) {
			        	if(result != null){
			        		MainActivity.mutelist = new long[result.getIDs().length];
			        		MainActivity.mutelist = result.getIDs();
			        	}else
			        	{
			        		MainActivity.mutelist = new long[1];
			        		MainActivity.mutelist[0] = 0;
			        	}
			        }
			    };
		    task.execute();
		}

	  //3. UserStream 受信時に応答する（UserStreamListener）リスナーを実装する
		private class MyUserStreamAdapter extends UserStreamAdapter {
		    // 新しいツイート（ステータス）を取得する度に呼び出される
			//int count;
			int position,y;
		    @Override
		    public void onStatus(final Status status) {
		    	Boolean mute = false;
		    	for(long ID : MainActivity.mutelist)
		    	{
		    		if((status.getUser().getId() == ID) && (sp.getBoolean("mute_flag", false)))
	        		{
	        			mute = true;
	        		}
		    	}
		    	if(!mute){
			    	//非同期リアルタイム更新
			    	AsyncTask<Void, Void, Boolean> task = new AsyncTask<Void, Void, Boolean>() {
				    	@Override
				        protected Boolean doInBackground(Void... params) {
				    		try {
								//ここは空白or適当な処理、戻り値は適当に
								//count = lv.getFirstVisiblePosition();
								position = lv.getFirstVisiblePosition();
								y = lv.getChildAt(0).getTop();
							    return true;
							} catch (Exception e) {
								e.printStackTrace();
								return false;
							}
				        }

					        @Override
					        protected void onPostExecute(Boolean result) {
					        	if(result){
						        	if(mAdapter.getPosition(status) == -1){
						        		if(sp.getBoolean("Streeming_stok", false) == false){
						        			if(TempTweet == null || TempTweet.getId() != status.getId()){
						        				mAdapter.insert(status, 0);
						        				MainActivity.TLStatusIDs.add(0, status);
//												putDBTweet(status);
						        				TempTweet = status;
						        			}
						        		lv.setSelectionFromTop(position + 1, y);
						        		}else if(TempTweet == null || TempTweet.getId() != status.getId())//マイペース更新
						        		{
						        			if(Streeming_stok.size() == 0){
												mAdapter.insert(null, 0);
												MainActivity.TLStatusIDs.add(0, null);
												lv.setSelectionFromTop(position + 1, y);
												if(sp.getBoolean("Streeming_Nof_Tost", false) == true){
													MainActivity.showToast("新着ツイートがあるみたい");
												}
						        			}
						        			Streeming_stok.add(status);
											//日付を書式に従って変換
											SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd-kk-mm-ss-SSS", Locale.JAPANESE);
											String datastring = sdf.format(status.getCreatedAt());
											ContentValues values = new ContentValues();
											values.put("tweet", status.getText());
											values.put("createdAt", datastring);
											values.put("tweetID", status.getId());
											values.put("tweetDel", "false");
											mydb.insert("TweetTable", null, values);
						        			Streeming_stok_Flug = true;
						        			TempTweet = status;
						        		}
						        	}
					        	}
					        }
					    };
					    task.execute();

				    //自分宛てのツイートか
			    	boolean RepNotifFlag = false;
			    	boolean ReoNotifNotFlag = false;
		    		for (UserMentionEntity UrlLink : status.getUserMentionEntities()) {
		    			if(UrlLink.getScreenName().equals(MainActivity.userName))
		    			{
		    				RepNotifFlag = true;
		    				if(status.getUser().getScreenName().equals(MainActivity.userName))
		    				{
		    					ReoNotifNotFlag = true;
		    				}
		    			}
		    		}
			        if(RepNotifFlag && !ReoNotifNotFlag && sp.getBoolean("NotificationMen", false)){
			        	MainActivity.notificationMention++;
			        	NotificationManager mNotificationManager = (NotificationManager) Activity.getSystemService(Context.NOTIFICATION_SERVICE);
			            mNotificationManager.cancelAll();
			            MainActivity.sendRepNotification("新しいリプライがあります。");
			        }
			        if(status.getRetweetedStatus().getUser().getId() == MainActivity.Userid && sp.getBoolean("NotificationRT", false)){
			        	MainActivity.notificationRetweet++;
			        	NotificationManager mNotificationManager = (NotificationManager) Activity.getSystemService(Context.NOTIFICATION_SERVICE);
			            mNotificationManager.cancelAll();
			            MainActivity.sendRepNotification("リツイートされました。");
			        }
		    	}
		    }

			@Override
			public void onFavorite(User source, User target, Status favoritedStatus) {
				super.onFavorite(source, target, favoritedStatus);
				if(!source.getScreenName().equals(MainActivity.userName) && sp.getBoolean("NotificationFav", false))
				{
					MainActivity.notificationFav++;
					NotificationManager mNotificationManager = (NotificationManager) Activity.getSystemService(Context.NOTIFICATION_SERVICE);
			        mNotificationManager.cancelAll();
			        MainActivity.sendRepNotification("お気に入りされました。");
				}
			}

			@Override
			public void onFollow(User source, User followedUser) {
				super.onFollow(source, followedUser);
				if(sp.getBoolean("NotificationFol", false)){
					MainActivity.notificationFollow++;
					NotificationManager mNotificationManager = (NotificationManager) Activity.getSystemService(Context.NOTIFICATION_SERVICE);
			        mNotificationManager.cancelAll();
			        MainActivity.sendRepNotification("@" + source.getScreenName() + " さんにフォローされました。");
				}
			}

			@Override
			public void onDirectMessage(DirectMessage directMessage) {
				super.onDirectMessage(directMessage);
				if(sp.getBoolean("NotificationDM", false)){
					MainActivity.notificationDM++;
					NotificationManager mNotificationManager = (NotificationManager) Activity.getSystemService(Context.NOTIFICATION_SERVICE);
			        mNotificationManager.cancelAll();
			        MainActivity.sendRepNotification("@" + directMessage.getSenderScreenName() + " さんからのDMがあります。");
				}
			}
		}

		//自分のIDとUser名取得
	    private void Get_My_ID() {
		    AsyncTask<Void, Void, User> task = new AsyncTask<Void, Void, User>() {
		    	@Override
		        protected User doInBackground(Void... params) {
		        	try {
						return mTwitter.verifyCredentials();//Userオブジェクトを作成
		            } catch (TwitterException e){
						e.printStackTrace();
					} catch(Exception e)
					{
						e.printStackTrace();
					}
		            return null;
		        }

		        @Override
		        protected void onPostExecute(User result) {
		        	if(result != null){
		        		MainActivity.Userid = result.getId();
		        		MainActivity.userName = result.getScreenName();
		        	}
		        }
		    };
		    task.execute();
		}

		@Override
		public void onDestroy() {
			// 使い終わったBroadcastReceiverを解放
			Activity.unregisterReceiver(connectivityActionReceiver);
			super.onDestroy();
			if(twitterStream != null){
				twitterStream.shutdown();
			}
			mydb.close();
			tdb.close();
		}

		@Override
		public void onResume() {
			super.onResume();
	        final boolean Streem_Flug = sp.getBoolean("Streem_Flug", false);
	        final boolean LongTap = sp.getBoolean("Tap_Setting", true);

			//ネットワーク状態の検出準備
			//初期状態取得
			ConnectivityManager cm = (ConnectivityManager) Activity.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo ni = cm.getActiveNetworkInfo();
			connected = (ni != null) && ni.isConnected();
			boolean fconnected = !connected;
			onConnectedChanged(fconnected,ni);

			// BroadcastReceiverを登録
			if(connectivityActionReceiver == null) {
				connectivityActionReceiver = new BroadcastReceiver() {
					@Override
					public void onReceive(Context context, Intent intent) {
						// ネットワーク接続状態を取得する
						ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
						NetworkInfo ni = cm.getActiveNetworkInfo();
						boolean connected = (ni != null) && ni.isConnected();

						// ハンドラをコールする
						onConnectedChanged(connected, ni);
					}
				};
				Activity.registerReceiver(connectivityActionReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
			}
			mAdapter.clear();	//バグ回避のため一度クリアにする
	        if(Streem_Flug){
	        	if(MainActivity.TLStatusIDs != null && mAdapter.getCount() == 0){
					if (Streeming_stok.size() != 0){
						if(MainActivity.TLStatusIDs != null && MainActivity.TLStatusIDs.get(0) != null){
							mAdapter.insert(null, 0);
							MainActivity.TLStatusIDs.add(0, null);
						}
					}
					if(MainActivity.TLStatusIDs != null){
						if(MainActivity.TLStatusIDs.size() != 0) {
							if(MainActivity.TLStatusIDs.get(0) == null) {
								MainActivity.TLStatusIDs.remove(0);
							}
						}
					}
	        	}
	        	if(twitterStream != null){
		        	// 4. TwitterStream に UserStreamListener を実装したインスタンスを設定する
		            twitterStream.addListener(mMyUserStreamAdapter);
		            // 5. TwitterStream#user() を呼び出し、ユーザーストリームを開始する
		            twitterStream.user();
	        	}
				if(MainActivity.TLStatusIDs.size() != 0){
					if(MainActivity.TLStatusIDs.get(0) == null){
						MainActivity.TLStatusIDs.remove(0);
					}
					for (Status status : MainActivity.TLStatusIDs) {
						mAdapter.add(status);
					}
				}
				mAdapter.notifyDataSetChanged();
	        }
	        else
	        {
	        	if(twitterStream != null){
	    			twitterStream.shutdown();
	    		}
	        	if(MainActivity.TLStatusIDs != null){
		        	if(MainActivity.TLStatusIDs.size() != 0){
		                for (Status status : MainActivity.TLStatusIDs) {
		                    mAdapter.add(status);
		                }
		        	}
	        	}
	        }

	        //ListViewのクリックリスナー登録
			lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
				//通常押し
				@Override
				public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
                    Boolean HFFlag = false; //ヘッダーorフッターが触られたかどうかを格納
					//フッターがクリックされた
					if(position != 0 && mAdapter.getItem(position) == null){
						NewReloadFulg = false;
						MainActivity.TLStatusIDs.remove(position);
			            reloadTimeLine();
                        HFFlag = true;
	                }
					//ヘッダーがクリックされた
					if(position == 0 && mAdapter.getItem(position) == null){
                        HFFlag = true;
						MainActivity.TLStatusIDs.remove(0);
						mAdapter.remove(null);
						if(Streem_Flug){
							int y = lv.getChildAt(0).getTop();
							int i = 0;
							for (Status status : Streeming_stok) {
								mAdapter.insert(status, 0);
				        		i++;
							}
							Streeming_stok.clear();	//追加し終わったら削除
							mAdapter.notifyDataSetChanged();
							lv.setSelectionFromTop(position + i, y);
						}else{
							NewReloadFulg = true;
				            reloadTimeLine();
						}
	                }

					if(LongTap == false && HFFlag == false){
						try {
							if(Streem_Flug){
								try {
									if(Streeming_stok.size() != 0){
										Tweet = mAdapter.getItem(position - 1);
									}else{
										Tweet = mAdapter.getItem(position);
									}
								} catch (Exception e) {
									e.printStackTrace();
									MainActivity.showDialog(e.getMessage());
								}
							}else
							{
								try {
									Tweet = mAdapter.getItem(position - 1);
								} catch (Exception e) {
									e.printStackTrace();
									MainActivity.showDialog(e.getMessage());
								}
							}
							List_Menu list = new List_Menu(Activity);
							list.Tweet_Menu(Tweet, mTwitter);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}

			});

			lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
				//長押し
				@Override
				public boolean onItemLongClick(AdapterView<?> parent, View view,int position, long id) {
					if(LongTap == true){
						try {
							if(Streem_Flug){
								try {
									if(Streeming_stok.size() != 0){
										Tweet = mAdapter.getItem(position);
									}else{
										Tweet = mAdapter.getItem(position);
									}
								} catch (Exception e) {
									e.printStackTrace();
									MainActivity.showDialog(e.getMessage());
								}

							}else
							{
								try {
									Tweet = mAdapter.getItem(position);
								} catch (Exception e) {
									e.printStackTrace();
									MainActivity.showDialog(e.getMessage());
								}
							}
							List_Menu list = new List_Menu(Activity);
							list.Tweet_Menu(Tweet, mTwitter);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					return true;
				}
			});
		}

	/**
	 * ネットワーク接続状態が変わった時にコールされるハンドラ
	 */
	private void onConnectedChanged(boolean connected,NetworkInfo ni) {
		if (connected == this.connected) return;

		this.connected = connected;

		if (connected && ni != null) {
			Log.d("test", "接続済み");
			switch (ni.getType()){
				case ConnectivityManager.TYPE_WIFI: // Wifi
					Log.d("test", "Wifi接続済み");
					if(sp.getBoolean("Streem_Flug", false) && twitterStream != null){
						// 4. TwitterStream に UserStreamListener を実装したインスタンスを設定する
						twitterStream.addListener(mMyUserStreamAdapter);
						// 5. TwitterStream#user() を呼び出し、ユーザーストリームを開始する
						twitterStream.user();
						Log.d("test", "Stream接続");
					}
					return;
				case ConnectivityManager.TYPE_MOBILE_DUN: // Mobile 3G
				case ConnectivityManager.TYPE_MOBILE_HIPRI:
				case ConnectivityManager.TYPE_MOBILE_MMS:
				case ConnectivityManager.TYPE_MOBILE_SUPL:
				case ConnectivityManager.TYPE_MOBILE:
					Log.d("test", "3G接続済み");
					if(twitterStream != null && sp.getBoolean("Streeming_Wifi", false)){
						twitterStream.shutdown();
						Log.d("test", "Stream切断");
					}
					return;
				case ConnectivityManager.TYPE_BLUETOOTH: // Bluetooth
					Log.d("test", "Bluetooth接続済み");
					return;
				case ConnectivityManager.TYPE_ETHERNET:  // Ethernet
					Log.d("test", "Ethernet接続済み");
					return;
				case ConnectivityManager.TYPE_WIMAX:
					Log.d("test", "WIMAX接続済み");
					return;
			}
		} else {
			Log.d("test", "networkから切断されました");
			if (twitterStream != null) {
				twitterStream.shutdown();
				Log.d("test", "Stream切断");
			}
		}
	}

	private void putDBTweet(Status status){
		//日付を書式に従って変換
		SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd-kk-mm-ss-SSS", Locale.JAPANESE);
		String datastring = sdf.format(status.getCreatedAt());
		ContentValues values = new ContentValues();
        String json = DataObjectFactory.getRawJSON(status);
		values.put("tweet",status.toString());
		values.put("createdAt", datastring);
		values.put("tweetID", status.getId());
		values.put("tweetDel", "false");
		mydb.insert("TweetTable", null, values);
	}
}
