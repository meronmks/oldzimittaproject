package com.meronmks.zimitta;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.ListActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.PaintDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

public class MentionToMentionLineActivity extends ListActivity {

	private TweetAdapter mAdapter;
    private Twitter mTwitter;
    private SharedPreferences sp;
    private long mentionID;
    private Boolean loadLock = false;
    private List<Status> MentionIDs = new ArrayList<Status>();
    private ListView lv;
    private ProgressBar progressBar;
    private  SharedPreferences accountIDCount;

    //定数
    public static final String cache = "tweetcache.txt";

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@SuppressLint("NewApi")
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PaintDrawable paintDrawable = new PaintDrawable(Color.argb(255,0,0,0));
        getWindow().setBackgroundDrawable(paintDrawable);
        sp = PreferenceManager.getDefaultSharedPreferences(this);
        accountIDCount = getSharedPreferences("accountidcount", 0);
        if (!TwitterUtils.hasAccessToken(this,accountIDCount.getLong("ID_Num_Now", 0))) {
            Intent intent = new Intent(this, TwitterOAuthActivity.class);
            startActivity(intent);
            finish();
        }else {
        	lv = getListView();
    		progressBar = new ProgressBar(this);
    		lv.addFooterView(progressBar);	//フッターにプログレスバーの設置
            mTwitter = TwitterUtils.getTwitterInstance(this,accountIDCount.getLong("ID_Num_Now", 0));
            mAdapter = new TweetAdapter(this);
            setListAdapter(mAdapter);

            final boolean LongTap = sp.getBoolean("Tap_Setting", true);

    		lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
    			//通常押し
    			@Override
    			public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
    				if(LongTap == false){
    					List_Menu list = new List_Menu(MentionToMentionLineActivity.this);
						list.Tweet_Menu(mAdapter.getItem(position), mTwitter);
    				}
    			}
    		});

    		lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
    			//長押し
    			@Override
    			public boolean onItemLongClick(AdapterView<?> parent, View view,int position, long id) {
    				if(LongTap == true){
    					List_Menu list = new List_Menu(MentionToMentionLineActivity.this);
						list.Tweet_Menu(mAdapter.getItem(position), mTwitter);
    				}
    				return true;
    			}
    		});



            Intent intent = getIntent();
            mentionID = intent.getLongExtra("StutusID", BIND_ABOVE_CLIENT);

            //会話検索
            final Timer timer = new Timer();
            timer.scheduleAtFixedRate(new TimerTask(){
		        @Override
		        public void run() {
		        	if(mentionID != 0){
				    	for(int i = 0; i<= MainActivity.myListStatusAdapter.size() - 1; i++){
				        	if(MainActivity.myListStatusAdapter.get(i).getId() == mentionID){
				        		mAdapter.add(MainActivity.myListStatusAdapter.get(i));
				        		MentionIDs.add(MainActivity.myListStatusAdapter.get(i));
				        		if(MainActivity.myListStatusAdapter.get(i).getInReplyToStatusId() != 0){
				        			mentionID = MainActivity.myListStatusAdapter.get(i).getInReplyToStatusId();
				        		}
				        	}
				        }
				        if(!loadLock){
				        	loadLock = true;
				        LoadMention();
				        }
		        	}else{
						timer.cancel();
		        	}
		        }
            }, 0, 1500);
        }
    }

	//会話の非同期処理
	private void LoadMention()	{
		AsyncTask<Void, Void,  Status> task = new AsyncTask<Void, Void, Status>() {
			//処理をここに書く
			@Override
	        protected twitter4j.Status doInBackground(Void... params) {
				try{
					return mTwitter.showStatus(mentionID);
				} catch (TwitterException e){
					e.printStackTrace();
				} catch(Exception e)
				{
					e.printStackTrace();
				}
				return null;
			}
			//処理が終わった後の処理
			@Override
	        protected void onPostExecute(twitter4j.Status result) {
				if (result != null) {
					mAdapter.add(result);
					MentionIDs.add(result);
					mentionID = result.getInReplyToStatusId();
					loadLock = false;
				}else{
					showToast("会話の追加取得終了");
					lv.removeFooterView(progressBar);	//フッターの削除
					mentionID = 0;
					loadLock = false;
				}
			}
		};
		task.execute();
	}

	private void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

}