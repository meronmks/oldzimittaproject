package com.meronmks.zimitta;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Button;
import twitter4j.*;

public class UserActionClass {

    Context context;
    Twitter mTwitter;
    public UserActionClass(Context c, Twitter t) {
        this.context = c;
        this.mTwitter = t;
    }

    public void tweetFav(Status status){

    }

    public void userFollow(final long userID){
        final AsyncTask<Void, Void, User> task = new AsyncTask<Void, Void, User>() {

            @Override
            protected User doInBackground(Void... params) {
                try {
                    return mTwitter.createFriendship(userID);
                } catch (TwitterException e) {
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(User resurt) {
                if(resurt != null){
                    MainActivity.showToast("フォロー完了");
                }else{
                    MainActivity.showToast("フォロー失敗");
                }
            }
        };
        task.execute();

    }

    public void userUnduFollow(final long userID){
        final AsyncTask<Void, Void, User> task = new AsyncTask<Void, Void, User>() {

            @Override
            protected User doInBackground(Void... params) {
                try {
                    return mTwitter.destroyFriendship(userID);
                } catch (TwitterException e) {
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(User resurt) {
                if(resurt != null){
                    MainActivity.showToast("リムーブ完了");
                }else{
                    MainActivity.showToast("リムーブ失敗");
                }
            }
        };
        task.execute();

    }

    public void chackFollow(final User user, final Button b){
        final long cursor = -1L;

        final AsyncTask<Void, Void, IDs> task = new AsyncTask<Void, Void, IDs>() {
            @Override
            protected IDs doInBackground(Void... params) {
                try {
                    return mTwitter.getFollowersIDs(cursor);
                } catch (TwitterException e) {
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(IDs ids) {
                if(ids != null){
                    Boolean machID = false;
                    for(long ID :ids.getIDs()){
                        if(ID == user.getId()){
                            machID = true;
                        }
                    }
                    if(machID) {
                        b.setText("フォロー済み");
                    }else{
                        b.setText("フォロー");
                    }
                }else{

                }
            }
        };
        task.execute();
    }
}
