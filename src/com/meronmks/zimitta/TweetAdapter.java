package com.meronmks.zimitta;

import java.util.Calendar;
import java.util.Date;

import twitter4j.DirectMessage;
import twitter4j.MediaEntity;
import twitter4j.Status;
import twitter4j.URLEntity;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.loopj.android.image.SmartImageView;

public class TweetAdapter extends ArrayAdapter<Status> {
	private LayoutInflater mInflater;
	private String TweetText;

    public TweetAdapter(Context context) {
        super(context, android.R.layout.simple_list_item_1);
        mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
    }

    static class ViewHolder {
        SmartImageView icon,rticon;
        TextView name;
        TextView screenName;
        TextView text;
        TextView time;
        TextView rt_To;
        TextView rt;
        TextView fav;
        TextView via;
        ImageView tweetStatus;
        RelativeLayout relativeLayout;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Status item = getItem(position);
        if(item != null) {
            ViewHolder holder;
            Date TimeStatusNow = null;
            Date TimeStatusTweet = null;
            Calendar cal1 = Calendar.getInstance();
            Calendar cal2 = Calendar.getInstance();
            Long My_ID = MainActivity.Userid;	//共有変数の呼び出し
            TimeStatusNow = new Date();
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.list_item_tweet, null);

                SmartImageView Icon = (SmartImageView) convertView.findViewById(R.id.icon);	//画像View
                SmartImageView RTIcon = (SmartImageView) convertView.findViewById(R.id.rticon);	//画像View
                TextView Name = (TextView) convertView.findViewById(R.id.name);		//名前View
                TextView ScreenName = (TextView) convertView.findViewById(R.id.screen_name);	//ＩＤView
                TextView Text = (TextView) convertView.findViewById(R.id.text);		//ツイート本文View
                TextView Time =(TextView) convertView.findViewById(R.id.time);	//投稿時間View
                TextView RT_To = (TextView) convertView.findViewById(R.id.RT_to);	//ＲＴした人View
                TextView RT =(TextView) convertView.findViewById(R.id.RT);		//RT数View
                TextView Fav =(TextView) convertView.findViewById(R.id.Fav);	//お気に入り数View
                TextView Via =(TextView) convertView.findViewById(R.id.via);	//投稿されたクライアント名Viwe
                ImageView TweetStatus = (ImageView) convertView.findViewById(R.id.TweetStatus);	//右の帯Viwe
                RelativeLayout RelativeLayout = (RelativeLayout) convertView.findViewById(R.id.Tweet_List);

                holder = new ViewHolder();
                holder.icon = Icon;
                holder.rticon = RTIcon;
                holder.name = Name;
                holder.screenName = ScreenName;
                holder.text = Text;
                holder.time = Time;
                holder.rt_To = RT_To;
                holder.rt = RT;
                holder.fav = Fav;
                holder.via = Via;
                holder.tweetStatus = TweetStatus;
                holder.relativeLayout = RelativeLayout;
                convertView.setTag(holder);

            } else {
                holder = (ViewHolder)convertView.getTag();
            }
            if(holder == null){
                convertView = mInflater.inflate(R.layout.list_item_tweet, null);

                SmartImageView Icon = (SmartImageView) convertView.findViewById(R.id.icon);	//画像View
                SmartImageView RTIcon = (SmartImageView) convertView.findViewById(R.id.rticon);	//画像View
                TextView Name = (TextView) convertView.findViewById(R.id.name);		//名前View
                TextView ScreenName = (TextView) convertView.findViewById(R.id.screen_name);	//ＩＤView
                TextView Text = (TextView) convertView.findViewById(R.id.text);		//ツイート本文View
                TextView Time =(TextView) convertView.findViewById(R.id.time);	//投稿時間View
                TextView RT_To = (TextView) convertView.findViewById(R.id.RT_to);	//ＲＴした人View
                TextView RT =(TextView) convertView.findViewById(R.id.RT);		//RT数View
                TextView Fav =(TextView) convertView.findViewById(R.id.Fav);	//お気に入り数View
                TextView Via =(TextView) convertView.findViewById(R.id.via);	//投稿されたクライアント名Viwe
                ImageView TweetStatus = (ImageView) convertView.findViewById(R.id.TweetStatus);	//右の帯Viwe
                RelativeLayout RelativeLayout = (RelativeLayout) convertView.findViewById(R.id.Tweet_List);

                holder = new ViewHolder();
                holder.icon = Icon;
                holder.rticon = RTIcon;
                holder.name = Name;
                holder.screenName = ScreenName;
                holder.text = Text;
                holder.time = Time;
                holder.rt_To = RT_To;
                holder.rt = RT;
                holder.fav = Fav;
                holder.via = Via;
                holder.tweetStatus = TweetStatus;
                holder.relativeLayout = RelativeLayout;
                convertView.setTag(holder);
            }
            holder.icon.setVisibility(View.VISIBLE);
            holder.name.setVisibility(View.VISIBLE);
            holder.screenName.setVisibility(View.VISIBLE);
            holder.text.setVisibility(View.VISIBLE);
            holder.time.setVisibility(View.VISIBLE);
            holder.rt_To.setVisibility(View.VISIBLE);
            holder.rt.setVisibility(View.VISIBLE);
            holder.fav.setVisibility(View.VISIBLE);
            holder.via.setVisibility(View.VISIBLE);
            holder.tweetStatus.setVisibility(View.INVISIBLE);
            //RTされたツイートかどうか
            if (item.getRetweetedStatus() == null) {
                holder.icon.setImageUrl(item.getUser().getProfileImageURLHttps());
                holder.name.setText(item.getUser().getName());
                holder.screenName.setText("@" + item.getUser().getScreenName());
                TweetText = item.getText();
                //短縮URL置換
                MediaEntity[] ImgLink = null;
                if (item.getMediaEntities() != null) {
                    ImgLink = item.getMediaEntities();
                } else if (item.getExtendedMediaEntities() != null) {
                    ImgLink = item.getExtendedMediaEntities();
                }
                if (ImgLink.length != 0) {
                    for (int i = 0; i < ImgLink.length; i++) {
                        TweetText = TweetText.replaceAll(ImgLink[i].getURL(), ImgLink[i].getMediaURL());
                    }
                }

                URLEntity[] UrlLink = null;
                if (item.getURLEntities() != null) {
                    UrlLink = item.getURLEntities();
                } else if (item.getExtendedMediaEntities() != null) {
                    UrlLink = item.getURLEntities();
                }
                for (int i = 0; i < UrlLink.length; i++) {
                    TweetText = TweetText.replaceAll(UrlLink[i].getURL(), UrlLink[i].getExpandedURL());
                }

                holder.text.setText(TweetText);
                TimeStatusTweet = item.getCreatedAt();
                cal1.setTime(TimeStatusTweet);
                cal2.setTime(TimeStatusNow);
                long date1 = cal1.getTimeInMillis();
                long date2 = cal2.getTimeInMillis();
                long time = (date2 - date1) / 1000;
                if (time <= 59) {
                    holder.time.setText(time + "s前");
                }
                time = time / 60;
                if ((time <= 59) && (time >= 1)) {
                    holder.time.setText(time + "m前");
                }
                time = time / 60;
                if ((time <= 23) && (time >= 1)) {
                    holder.time.setText(time + "h前");
                }
                time = time / 24;
                if (time != 0) {
                    holder.time.setText(item.getCreatedAt().toString());
                }
                holder.rt_To.setVisibility(View.GONE);    //RTした人の名前を非表示
                holder.rticon.setVisibility(View.GONE);
            } else {
                holder.icon.setImageUrl(item.getRetweetedStatus().getUser().getProfileImageURLHttps());
                holder.rticon.setImageUrl(item.getUser().getBiggerProfileImageURLHttps());
                holder.name.setText(item.getRetweetedStatus().getUser().getName());
                holder.screenName.setText("@" + item.getRetweetedStatus().getUser().getScreenName());
                //短縮URL置換
                MediaEntity[] ImgLink = null;
                if (item.getRetweetedStatus().getMediaEntities() != null) {
                    ImgLink = item.getRetweetedStatus().getMediaEntities();
                } else if (item.getRetweetedStatus().getExtendedMediaEntities() != null) {
                    ImgLink = item.getRetweetedStatus().getExtendedMediaEntities();
                }
                TweetText = item.getRetweetedStatus().getText();
                if (ImgLink.length != 0) {
                    for (int i = 0; i < ImgLink.length; i++) {
                        TweetText = TweetText.replaceAll(ImgLink[i].getURL(), ImgLink[i].getMediaURL());
                    }
                }

                URLEntity[] UrlLink = null;
                if (item.getRetweetedStatus().getURLEntities() != null) {
                    UrlLink = item.getRetweetedStatus().getURLEntities();
                } else if (item.getRetweetedStatus().getExtendedMediaEntities() != null) {
                    UrlLink = item.getRetweetedStatus().getURLEntities();
                }
                for (int i = 0; i < UrlLink.length; i++) {
                    TweetText = TweetText.replaceAll(UrlLink[i].getURL(), UrlLink[i].getExpandedURL());
                }
                holder.text.setText(TweetText);
                TimeStatusTweet = item.getRetweetedStatus().getCreatedAt();
                cal1.setTime(TimeStatusTweet);
                cal2.setTime(TimeStatusNow);
                long date1 = cal1.getTimeInMillis();
                long date2 = cal2.getTimeInMillis();
                long time = (date2 - date1) / 1000;
                if (time <= 59) {
                    holder.time.setText(time + "s前");
                }
                time = time / 60;
                if ((time <= 59) && (time >= 1)) {
                    holder.time.setText(time + "m前");
                }
                time = time / 60;
                if ((time <= 23) && (time >= 1)) {
                    holder.time.setText(time + "h前");
                }
                time = time / 24;
                if (time != 0) {
                    holder.time.setText(item.getRetweetedStatus().getCreatedAt().toString());
                }
                holder.rt_To.setVisibility(View.VISIBLE);
                holder.rt_To.setText(item.getUser().getName() + " さんがリツイート");
                holder.tweetStatus.setVisibility(View.VISIBLE);
                holder.tweetStatus.setBackgroundColor(Color.GREEN);
                holder.rticon.setVisibility(View.VISIBLE);
            }
            holder.rt.setText("RT:" + item.getRetweetCount());
            holder.fav.setText("Fav:" + item.getFavoriteCount());
            String str = item.getSource();
            str = str.replaceAll("<.+?>", "");
            holder.via.setText(str + "：より");
            //自分のツイートか
            if (item.getUser().getId() == My_ID) {
                holder.tweetStatus.setVisibility(View.VISIBLE);
                holder.tweetStatus.setBackgroundColor(Color.CYAN);
            }
            //自分宛てのツイートか
            if (item.getUserMentionEntities() != null) {
                for (int i = 0; i < item.getUserMentionEntities().length; i++) {
                    if (item.getUserMentionEntities()[i].getScreenName().equals(MainActivity.userName)) {
                        holder.tweetStatus.setVisibility(View.VISIBLE);
                        holder.tweetStatus.setBackgroundColor(Color.RED);
                    }
                }
            }
            holder.relativeLayout.setBackgroundResource(R.drawable.listitem_color);
        }else{
            convertView = mInflater.inflate(R.layout.list_item_null, null);
        }
        return convertView;
    }


}