package com.meronmks.zimitta;

import android.annotation.SuppressLint;
import android.app.*;
import android.app.Notification.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.UserList;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends SherlockFragmentActivity {

    private SharedPreferences accountIDCount;
    private ShowRateLimit limit;

    //一時保存用変数群
    public static int notificationDM;
    public static int notificationFollow;
    public static int notificationMention;
    public static int notificationRetweet;
    public static int notificationFav;
    public static long Userid;
    public static String userName;
    public static List<Status> TLStatusIDs;
    public static List<Status> MenStatusIDs;
    public static long mutelist[];
    public static ArrayList<Status> myListStatusAdapter;
    public static ResponseList<UserList> ListTimeLineIDs;
    public static List<twitter4j.DirectMessage> DMStatusIDs;

    //デバックモードフラグ
    public static Boolean DebugMode = false;

    //プログレスバー格納用
    private static ProgressBar progres;

    private static Context act;

    //定数
    public static final String PREF_NAME = "twitter_access_token";
    public static final String TOKEN = "token";
    public static final String TOKEN_SECRET = "token_secret";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //バーの無いテーマに変更
        setTheme(R.style.Theme_Sherlock_NoActionBar);

        //初期化
        act = this;
        TLStatusIDs = new ArrayList<Status>();
        MenStatusIDs = new ArrayList<Status>();
        DMStatusIDs = new ArrayList<twitter4j.DirectMessage>();
        myListStatusAdapter = new ArrayList<Status>();
        ListTimeLineIDs = null;
        limit = new ShowRateLimit(this);

        setContentView(R.layout.activity_main);

        progres = (ProgressBar) findViewById(R.id.progressBar);
        //アカウント情報を読み込む
        accountIDCount = getSharedPreferences("accountidcount", 0);

        //アクセストークンがあるかどうか
        if (!TwitterUtils.hasAccessToken(this, accountIDCount.getLong("ID_Num_Now", 0))) {
            Intent intent = new Intent(this, TwitterOAuthActivity.class);
            intent.putExtra("Flag", false);
            startActivity(intent);
            finish();
        } else {
            //ボタン準備
            ImageButton tweet = (ImageButton) findViewById(R.id.tweet);
            ImageButton menu = (ImageButton) findViewById(R.id.Menu_button);

            //ボタンクリック時の動作設定
            tweet.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // クリックの処理を実行する
                    Intent intent = new Intent(MainActivity.this, TweetActivity.class);
                    startActivity(intent);
                }

            });

            menu.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // クリックの処理を実行する
                    Main_menu();
                }

            });

            //Fragment準備
            android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
            MainTabFragmentPagerAdapter pagerAdapter = new MainTabFragmentPagerAdapter(fragmentManager);
            ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
            viewPager.setAdapter(pagerAdapter);
        }
    }

    //リストメニュー
    protected void Main_menu() {
        String[] dialogItem;
        if(DebugMode) {
            dialogItem = new String[]{"プロフィール表示", "アカウント切替と追加", "連携アプリ", "設定","ShowLimit"};    //メニューの項目作り
        }else{
            dialogItem = new String[]{"プロフィール表示", "アカウント切替と追加", "連携アプリ", "設定"};    //メニューの項目作り
        }
        AlertDialog.Builder dialogMenu = new AlertDialog.Builder(MainActivity.this);
        dialogMenu.setItems(dialogItem, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        Intent My_Prof_Intent = new Intent(MainActivity.this, Prof_Activiti.class);
                        My_Prof_Intent.putExtra("UserID", Userid);
                        startActivity(My_Prof_Intent);
                        break;
                    case 1:
                        Intent Account = new Intent(MainActivity.this, AccountChangeActivity.class);
                        startActivity(Account);
                        break;
                    case 2:
                        Intent SettingsApplications = new Intent(MainActivity.this, WebTwitterLoginActivity.class);
                        startActivity(SettingsApplications);
                        break;
                    case 3:
                        Intent setting = new Intent(MainActivity.this, SettingActivity.class);
                        startActivity(setting);
                        break;
                    case 4:
                        limit.DebugMode();
                        break;
                    default:
                        showToast("Menuで例外発生");
                        break;
                }
            }
        }).create().show();
    }

    //トースト表示
    public static void showToast(String text) {
        Toast.makeText(act, text, Toast.LENGTH_SHORT).show();
    }

    //通知のメゾット
    @SuppressLint("NewApi")
    @SuppressWarnings("deprecation")
    public static void sendRepNotification(String text) {
        Builder n; // Notificationの生成
//  	    n.icon = R.drawable.ic_launcher; // アイコンの設定
//  	    n.tickerText = text; // メッセージの設定
        //
        Intent i = new Intent(act.getApplicationContext(), MainActivity.class);
        PendingIntent pi = PendingIntent.getActivity(act, 0, i, 0);
//  	    n.setLatestEventInfo(getApplicationContext(), "じみったー", text, pi);

        n = new Builder(act)
                .setContentIntent(pi).setSmallIcon(R.drawable.icon)
                .setTicker(text).setContentTitle(text)
                .setContentText("返信:" + notificationMention + " RT:" + notificationRetweet + " お気に入り:" + notificationFav + " DM:" + notificationDM + " フォロー:" + notificationFollow).setWhen(System.currentTimeMillis());
        // 通知時の音・バイブ・ライト
        n.setDefaults(Notification.DEFAULT_SOUND
                | Notification.DEFAULT_VIBRATE
                | Notification.DEFAULT_LIGHTS);

        // NotificationManagerのインスタンス取得
        NotificationManager nm = (NotificationManager) act.getSystemService(Service.NOTIFICATION_SERVICE);
        nm.notify(1, n.build()); // 設定したNotificationを通知する
    }

    //エラーダイアログ
    public static void showDialog(String text) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(act);
        alertDialog.setTitle("Error!");      //タイトル設定
        alertDialog.setMessage(text);  //内容(メッセージ)設定

        // OK(肯定的な)ボタンの設定
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // OKボタン押下時の処理
            }
        });
        alertDialog.show();
    }

    /**読み込み表示**/
    public static void progresRun() {
        progres.setIndeterminate(true);
        progres.setVisibility(View.VISIBLE);
    }

    /**読み込み表示終了**/
    public static void progresStop() {
        progres.setIndeterminate(false);
        progres.setVisibility(View.INVISIBLE);
    }
}
