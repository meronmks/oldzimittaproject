package com.meronmks.zimitta;

import java.io.File;
import java.io.InputStream;

import android.os.Build;
import android.view.Window;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class TweetActivity extends SherlockFragmentActivity {

    private EditText mInputText;
    private Twitter mTwitter;
    private ProgressDialog progressDialog;
    private String path1 = null;
    private String path2 = null;
    private String path3 = null;
    private String path4 = null;
	Uri uri1 = null;
	Uri uri2 = null;
	Uri uri3 = null;
	Uri uri4 = null;
    private TextView textCount;
    private  SharedPreferences accountIDCount;
    ImageButton button1;
    ImageButton button2;
    ImageButton button3;
    ImageButton button4;

    @SuppressLint("NewApi")
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_tweet);

		getSupportActionBar().setDisplayShowHomeEnabled(false);
        accountIDCount = getSharedPreferences("accountidcount", 0);
        mTwitter = TwitterUtils.getTwitterInstance(this,accountIDCount.getLong("ID_Num_Now", 0));

        mInputText = (EditText) findViewById(R.id.input_text);
        textCount = ((TextView)findViewById(R.id.textCount));

        textCount.setText(Integer.toString(140 - mInputText.getText().length()));

        mInputText.addTextChangedListener(new TextWatcher(){
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count){
                int textColor = Color.GRAY;

                // 入力文字数の表示
                int txtLength = 140 - s.length();
                textCount.setText(Integer.toString(txtLength) + "");

                // 指定文字数オーバーで文字色を赤くする
                if (txtLength < 0) {
                    textColor = Color.RED;
                }
                textCount.setTextColor(textColor);
            }

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				// TODO 自動生成されたメソッド・スタブ

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO 自動生成されたメソッド・スタブ

			}
        });

        // ビュー
        final View view = this.findViewById(R.id.Activity_Tweet);
        view.setBackgroundColor(Color.BLACK);
        findViewById(R.id.Image_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            	Intent intent = new Intent(Intent.ACTION_PICK);
        		intent.setType("image/*");
        		startActivityForResult(intent, 1);
            }
        });

        findViewById(R.id.action_tweet).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            	ShowprogressDialog();
                tweet();
            }
        });

        //画像取り消し用ボタン関連
        button1 = (ImageButton)findViewById(R.id.button1);
        button2 = (ImageButton)findViewById(R.id.Button02);
        button3 = (ImageButton)findViewById(R.id.Button03);
        button4 = (ImageButton)findViewById(R.id.Button04);

        //ボタンのクリックリスナの作成
        button1.setOnClickListener(new View.OnClickListener() {

    	    @Override
    	    public void onClick(View v) {
    	        // クリックの処理を実行する
    	    	button1.setVisibility(View.GONE);
    	    	path1 = null;
    	    	ImageView Select_Image_button = (ImageView) TweetActivity.this.findViewById(R.id.Select_Image_button);
				Select_Image_button.setImageResource(R.drawable.clear);
    	    }

    	});

        button2.setOnClickListener(new View.OnClickListener() {

    	    @Override
    	    public void onClick(View v) {
    	        // クリックの処理を実行する
    	    	button2.setVisibility(View.GONE);
    	    	path2 = null;
    	    	ImageView Select_Image_button = (ImageView) TweetActivity.this.findViewById(R.id.Select_Image_button1);
    	    	Select_Image_button.setImageResource(R.drawable.clear);
    	    }

    	});

        button3.setOnClickListener(new View.OnClickListener() {

    	    @Override
    	    public void onClick(View v) {
    	        // クリックの処理を実行する
    	    	button3.setVisibility(View.GONE);
    	    	path4 = null;
    	    	ImageView Select_Image_button = (ImageView) TweetActivity.this.findViewById(R.id.Select_Image_button2);
    	    	Select_Image_button.setImageResource(R.drawable.clear);
    	    }

    	});

        button4.setOnClickListener(new View.OnClickListener() {

    	    @Override
    	    public void onClick(View v) {
    	        // クリックの処理を実行する
    	    	button4.setVisibility(View.GONE);
    	    	path3 = null;
    	    	ImageView Select_Image_button = (ImageView) TweetActivity.this.findViewById(R.id.Select_Image_button3);
    	    	Select_Image_button.setImageResource(R.drawable.clear);
    	    }

    	});

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

  	  super.onActivityResult(requestCode, resultCode, data);

  	  String[] columns;
	columns = new String[]{ MediaStore.Images.Media.DATA };
  	  if (resultCode != RESULT_OK) return;

  	  if (requestCode == 1) {
			if(path1 == null){
			    uri1= data.getData();	//選択した画像の受け取り
			    ContentResolver cr = getContentResolver();
				if(uri1 != null)
				{
					Cursor c = cr.query(uri1, columns, null, null, null);
					c.moveToFirst();
					path1 = c.getString(0);
				}else{
					showDialog("画像受け取りに失敗しました。\nUriがnullです。");
				}
			}else if(path2 == null)
			{
				uri2= data.getData();	//選択した画像の受け取り
			    ContentResolver cr = getContentResolver();
				if(uri2 != null)
				{
					Cursor c = cr.query(uri2, columns, null, null, null);
					c.moveToFirst();
					path2 = c.getString(0);
				}else{
					showDialog("画像受け取りに失敗しました。\nUriがnullです。");
				}
			}else if(path3 == null)
			{
				uri3= data.getData();	//選択した画像の受け取り
			    ContentResolver cr = getContentResolver();
				if(uri3 != null){
					Cursor c = cr.query(uri3, columns, null, null, null);
					c.moveToFirst();
					path3 = c.getString(0);
				}else{
					showDialog("画像受け取りに失敗しました。\nUriがnullです。");
				}
			}else if(path4 == null)
			{
				uri4= data.getData();	//選択した画像の受け取り
			    ContentResolver cr = getContentResolver();
				if(uri4 != null)
				{
					Cursor c = cr.query(uri4, columns, null, null, null);
					c.moveToFirst();
					path4 = c.getString(0);
				}else{
					showDialog("画像受け取りに失敗しました。\nUriがnullです。");
				}
			}
			Button Image_button = (Button)this.findViewById(R.id.Image_button);
			Image_button.setText("さらに選択する");
			//画像の表示
			try {
				if(path4 != null){
					InputStream in = getContentResolver().openInputStream(uri4);
					Bitmap img = BitmapFactory.decodeStream(in);
					in.close();
					ImageView Select_Image_button = (ImageView) this.findViewById(R.id.Select_Image_button2);
					Select_Image_button.setImageBitmap(img);
					button3.setVisibility(View.VISIBLE);
				}
				if(path3 != null)
				{
					InputStream in = getContentResolver().openInputStream(uri3);
					Bitmap img = BitmapFactory.decodeStream(in);
					in.close();
					ImageView Select_Image_button = (ImageView) this.findViewById(R.id.Select_Image_button3);
					Select_Image_button.setImageBitmap(img);
					button4.setVisibility(View.VISIBLE);
				}
				if(path2 != null)
				{
					InputStream in = getContentResolver().openInputStream(uri2);
					Bitmap img = BitmapFactory.decodeStream(in);
					in.close();
					ImageView Select_Image_button = (ImageView) this.findViewById(R.id.Select_Image_button1);
					Select_Image_button.setImageBitmap(img);
					button2.setVisibility(View.VISIBLE);
				}
				if(path1 != null){
					InputStream in = getContentResolver().openInputStream(uri1);
					Bitmap img = BitmapFactory.decodeStream(in);
					in.close();
					ImageView Select_Image_button = (ImageView) this.findViewById(R.id.Select_Image_button);
					Select_Image_button.setImageBitmap(img);
					button1.setVisibility(View.VISIBLE);
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
  	  }
  }

    private void tweet() {
        AsyncTask<String, Void, Boolean> task = new AsyncTask<String, Void, Boolean>() {
        	String tmp = null;
            @Override
            protected Boolean doInBackground(String... params) {
                try {
                	if(path1 == null && path2 == null && path3 == null && path4 == null)
                	{
                		mTwitter.updateStatus(params[0]);
                		return true;
                	}
	                	StatusUpdate update = new StatusUpdate(params[0]);
	            		int i = 0;
	            		if(path1 != null)
	            		{
	            			i++;
	            		}
	            		if(path2 != null)
	            		{
	            			i++;
	            		}
	            		if(path3 != null)
	            		{
	            			i++;
	            		}
	            		if(path4 != null)
	            		{
	            			i++;
	            		}
	            		long media[] = new long[8];
	            		i=0;
	            		if(path1 != null)
	            		{
	            			media[i] = mTwitter.uploadMedia(new File(path1)).getMediaId();
	            			i++;
	            		}
	            		if(path2 != null)
	            		{
	            			media[i] = mTwitter.uploadMedia(new File(path2)).getMediaId();
	            			i++;
	            		}
	            		if(path3 != null)
	            		{
	            			media[i] = mTwitter.uploadMedia(new File(path3)).getMediaId();
	            			i++;
	            		}
	            		if(path4 != null)
	            		{
	            			media[i] = mTwitter.uploadMedia(new File(path4)).getMediaId();
	            			i++;
	            		}
					update.setMediaIds(media);
					mTwitter.updateStatus(update);
                    return true;
                } catch (TwitterException e) {
                	progressDialog.dismiss();
                    e.printStackTrace();
                    tmp = e.getMessage();
                    return false;
                }
            }

            @Override
            protected void onPostExecute(Boolean result) {
					if (result) {
                	progressDialog.dismiss();
                    showToast("ツイートが完了しました！");
                    finish();
                } else {
                	progressDialog.dismiss();
                    showDialog(tmp);
                }
            }
        };
        task.execute(mInputText.getText().toString());
    }

  //通信中とかのダイアログをだす
    private void ShowprogressDialog() {
		progressDialog = new ProgressDialog(TweetActivity.this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(true);
        progressDialog.setMessage("送信中・・・");
        progressDialog.setCancelable(false);
        progressDialog.show();
	}

    private void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

	//エラーダイアログ
	private void showDialog(String text){
		AlertDialog.Builder alertDialog=new AlertDialog.Builder(this);
		alertDialog.setTitle("Error!");      //タイトル設定
        alertDialog.setMessage(text);  //内容(メッセージ)設定

        // OK(肯定的な)ボタンの設定
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // OKボタン押下時の処理
            }
        });
        alertDialog.show();
	}
}