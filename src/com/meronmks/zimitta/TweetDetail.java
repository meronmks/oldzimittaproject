package com.meronmks.zimitta;

import java.util.Calendar;
import java.util.Date;

import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.UserMentionEntity;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.image.SmartImageView;

public class TweetDetail extends Activity implements OnClickListener {

	private Status Tweet;
	private Twitter mTwitter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tweet_detail);
		Date TimeStatusNow = new Date();
        Date TimeStatusTweet = null;
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        Intent intent = getIntent();
        Tweet = (Status)intent.getSerializableExtra("Detail");
        mTwitter = (Twitter)intent.getSerializableExtra("Twitter");

        //ボタン設定
  		ImageButton mention_button = (ImageButton)findViewById(R.id.mentionimageButton);
  		ImageButton rt_button = (ImageButton)findViewById(R.id.retweetImageButton);
  		ImageButton fav_button = (ImageButton)findViewById(R.id.favImageButton);
  		ImageButton menu_button = (ImageButton)findViewById(R.id.menuImageButton);

  		//ボタンのクリックリスナの作成
  		mention_button.setOnClickListener(this);
  		rt_button.setOnClickListener(this);
  		fav_button.setOnClickListener(this);
  		menu_button.setOnClickListener(this);

  		//テキスト等の設定変更準備
  		SmartImageView icon = (SmartImageView)findViewById(R.id.icon);
  		TextView name = (TextView) findViewById(R.id.name);		//名前View
        TextView screenName = (TextView) findViewById(R.id.screen_name);	//ＩＤView
        TextView text = (TextView) findViewById(R.id.text);		//ツイート本文View
        TextView Time =(TextView) findViewById(R.id.time);	//投稿時間View
        TextView RT_To = (TextView) findViewById(R.id.RT_to);	//ＲＴした人View
        TextView RT =(TextView) findViewById(R.id.RT);		//RT数View
        TextView Fav =(TextView) findViewById(R.id.Fav);	//お気に入り数View
        TextView Via =(TextView) findViewById(R.id.via);	//投稿元クライアント名Viwe
		TextView RTvia = (TextView) findViewById(R.id.RTvia);	//RTしたクライアント名

        //RTされているツイートかどうか？
        if(Tweet.getRetweetedStatus() == null)
        {
        	icon.setImageUrl(Tweet.getUser().getProfileImageURLHttps());
        	name.setText(Tweet.getUser().getName());
        	screenName.setText( "@" + Tweet.getUser().getScreenName());
        	text.setText(Tweet.getText());
	        TimeStatusTweet = Tweet.getCreatedAt();
	        cal1.setTime(TimeStatusTweet);
	        cal2.setTime(TimeStatusNow);
	        long date1 = cal1.getTimeInMillis();
	        long date2 = cal2.getTimeInMillis();
	        long time = (date2-date1)/1000;
	        if(time <= 59)
	        {
	        	Time.setText(time + "s前");
	        }
	        time = time / 60;
	        if((time <= 59) && (time >= 1))
	        {
	        	Time.setText(time + "m前");
	        }
	        time = time / 60;
	        if((time <= 23) && (time >= 1))
	        {
	        	Time.setText(time + "h前");
	        }
	        time = time / 24;
	        if (time != 0)
	        {
	        	Time.setText(Tweet.getCreatedAt().toString());
	        }
	        RT_To.setVisibility(View.GONE);	//RTした人の名前を非表示
			RTvia.setVisibility(View.GONE);
        }
        else
        {
        	icon.setImageUrl(Tweet.getRetweetedStatus().getUser().getProfileImageURL());
        	name.setText(Tweet.getRetweetedStatus().getUser().getName());
        	screenName.setText( "@" + Tweet.getRetweetedStatus().getUser().getScreenName());
        	text.setText(Tweet.getRetweetedStatus().getText());
	        TimeStatusTweet = Tweet.getRetweetedStatus().getCreatedAt();
	        cal1.setTime(TimeStatusTweet);
	        cal2.setTime(TimeStatusNow);
	        long date1 = cal1.getTimeInMillis();
	        long date2 = cal2.getTimeInMillis();
	        long time = (date2-date1)/1000;
	        if(time <= 59)
	        {
	        	Time.setText(time + "s前");
	        }
	        time = time / 60;
	        if((time <= 59) && (time >= 1))
	        {
	        	Time.setText(time + "m前");
	        }
	        time = time / 60;
	        if((time <= 23) && (time >= 1))
	        {
	        	Time.setText(time + "h前");
	        }
	        time = time / 24;
	        if (time != 0)
	        {
	        	Time.setText(Tweet.getRetweetedStatus().getCreatedAt().toString());
	        }
	        RT_To.setVisibility(View.VISIBLE);
	        RT_To.setText(Tweet.getUser().getName() + " さんがリツイート");
			RTvia.setVisibility(View.VISIBLE);
			String str1 = Tweet.getRetweetedStatus().getSource();
			str1 = str1.replaceAll("<.+?>", "");
			RTvia.setText(str1 + "：RT元via");
        }
        RT.setText("RT:" + Tweet.getRetweetCount());
        Fav.setText("Fav:" + Tweet.getFavoriteCount());
        String str = Tweet.getSource();
        str = str.replaceAll("<.+?>", "");
        Via.setText(str + "：より");
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
        case R.id.mentionimageButton:
    		Intent intent = new Intent(TweetDetail.this, MentionsActivity.class);
    		intent.putExtra("mentionID", Tweet.getId());
    		intent.putExtra("StatusID", Tweet.getUser().getScreenName());
    		intent.putExtra("Name", Tweet.getUser().getName());
    		intent.putExtra("Tweet", Tweet.getText());
    		intent.putExtra("Image", Tweet.getUser().getProfileImageURL());
    		String[] name = new String[20];
    		int j = 0;
    		for (UserMentionEntity UrlLink : Tweet.getUserMentionEntities()) {
    			name[j] = UrlLink.getScreenName();
    			j++;
    		}
    		intent.putExtra("UserMentionEntities", name);
            startActivity(intent);
            break;
        case R.id.retweetImageButton:
    		new AlertDialog.Builder(TweetDetail.this)
    		.setTitle("RTしてよろしいですか？")
    		.setPositiveButton(
    		"はい",
    		new DialogInterface.OnClickListener() {
    			public void onClick(DialogInterface dialog, int which) {
    				// OK時の処理
    				RTPost();
    			}
    		})
    		.setNegativeButton(
    		"いいえ",
    		new DialogInterface.OnClickListener() {
    			public void onClick(DialogInterface dialog, int which) {
    				// NO時の処理
    			}
    		})
    		.show();
            break;
		case R.id.favImageButton:
    		new AlertDialog.Builder(TweetDetail.this)
    		.setTitle("ふぁぼしますか？")
    		.setPositiveButton(
    		"はい",
    		new DialogInterface.OnClickListener() {
    			public void onClick(DialogInterface dialog, int which) {
    				// OK時の処理
    				FaPost();
    			}
    		})
    		.setNegativeButton(
    		"いいえ",
    		new DialogInterface.OnClickListener() {
    			public void onClick(DialogInterface dialog, int which) {
    				// NO時の処理
    			}
    		})
    		.show();
			break;
		case R.id.menuImageButton:
			List_Menu list = new List_Menu(TweetDetail.this);
			list.Detail_Menu(Tweet, mTwitter);
		    break;
      }
	}

	//RTの非同期処理
	private void RTPost()	{
		AsyncTask<Void, Void,  Boolean> task = new AsyncTask<Void, Void, Boolean>() {
			//処理をここに書く
			@Override
	        protected Boolean doInBackground(Void... params) {
				try{
					mTwitter.retweetStatus(Tweet.getId());
					return true;

				} catch (TwitterException e){
					e.printStackTrace();
				} catch(Exception e)
				{
					e.printStackTrace();
				}
				return false;
			}
			//処理が終わった後の処理
			@Override
	        protected void onPostExecute(Boolean result) {
				if (result != false) {
					showToast("リツイートしました。");
	            } else {
	                showToast("リツイートに失敗しました。。。");
	            }
			}
		};
		task.execute();
	}

	//お気に入りの非同期処理
	private void FaPost()	{
		AsyncTask<Void, Void,  Boolean> task = new AsyncTask<Void, Void, Boolean>() {
			//処理をここに書く
			@Override
	        protected Boolean doInBackground(Void... params) {
				try{
					mTwitter.createFavorite(Tweet.getId());
					return true;
				} catch (TwitterException e){
					e.printStackTrace();
				} catch(Exception e)
				{
					e.printStackTrace();
				}
				return false;
			}
			//処理が終わった後の処理
			@Override
	        protected void onPostExecute(Boolean result) {
				if (result != false) {
					showToast("ふぁぼしました。");
	            } else {
	                showToast("ふぁぼ失敗・・・");
	            }
			}
		};
		task.execute();
	}

	private void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }
}
