package com.meronmks.zimitta;

import android.os.Bundle;
import com.actionbarsherlock.app.SherlockActivity;

public class About extends SherlockActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.about);
	}

	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		com.actionbarsherlock.view.MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.debugmode, menu);
		return true;
	}

	//メニューのアイテムを押したときの判別
	@Override
	public boolean onOptionsItemSelected(com.actionbarsherlock.view.MenuItem item) {
		switch (item.getItemId()) {
			case R.id.Debug_Menu:        //アカウント追加画面へ
				MainActivity.DebugMode = !MainActivity.DebugMode;
				if(MainActivity.DebugMode){
					MainActivity.showToast("デバックモードに入ります");
				}else{
					MainActivity.showToast("デバックモードを終了します");
				}
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
