package com.meronmks.zimitta;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.actionbarsherlock.app.SherlockListFragment;
import twitter4j.Paging;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.conf.ConfigurationBuilder;

import java.util.List;

public class MentionTimeLineFragment extends SherlockListFragment {

    private static TweetAdapter mAdapter;
    private Twitter mTwitter;
    private ShowRateLimit limit;
    private SharedPreferences sp, accountIDCount;
    private long OldStatus;
    private long NewStatus;
    private Boolean NewReloadFulg = true;
    private Context Activity;
    private ListView lv = null;
    private Status Tweet;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO 自動生成されたメソッド・スタブ
        super.onActivityCreated(savedInstanceState);

        //Fragmentではこのメゾット以外でこれを実行すると落ちる
        lv = getListView();

        mAdapter = new TweetAdapter(Activity);
        setListAdapter(mAdapter);

        if(MainActivity.MenStatusIDs != null) {
            if (MainActivity.MenStatusIDs.size() == 0) {
                reloadMentionTimeLine();
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        Activity = getActivity();

        sp = PreferenceManager.getDefaultSharedPreferences(Activity);
        accountIDCount = Activity.getSharedPreferences("accountidcount", 0);
        SharedPreferences spOuth = Activity.getSharedPreferences(MainActivity.PREF_NAME + accountIDCount.getLong("ID_Num_Now", 0), Context.MODE_PRIVATE);


        // Twitter4Jに対してOAuth情報を設定
        ConfigurationBuilder builder = new ConfigurationBuilder();
        {
            // アプリ固有の情報
            builder.setOAuthConsumerKey(getString(R.string.twitter_consumer_key));
            builder.setOAuthConsumerSecret(getString(R.string.twitter_consumer_secret));
            // アプリ＋ユーザー固有の情報
            builder.setOAuthAccessToken(spOuth.getString(MainActivity.TOKEN, null));
            builder.setOAuthAccessTokenSecret(spOuth.getString(MainActivity.TOKEN_SECRET, null));
        }

        mTwitter = TwitterUtils.getTwitterInstance(Activity, accountIDCount.getLong("ID_Num_Now", 0));
        limit = new ShowRateLimit(Activity);
    }

    //リプライのみのタイムラインの非同期取得
    private void reloadMentionTimeLine() {
        MainActivity.progresRun();
        AsyncTask<Void, Void, List<Status>> task = new AsyncTask<Void, Void, List<Status>>() {
            String exception;
            int position, y;

            @Override
            protected List<twitter4j.Status> doInBackground(Void... params) {
                int i = Integer.parseInt(sp.getString("Load_Tweet", "20"));    //設定をStringで受け取り、intに変換
                if ((NewStatus == 0) && (NewReloadFulg)) {    //ツイートの読み込み数が0で新たに読み込むとき
                    try {
                        Paging p = new Paging();
                        p.count(i);
                        return mTwitter.getMentionsTimeline(p);
                    } catch (TwitterException e) {
                        e.printStackTrace();
                        exception = e.getMessage();
                    } catch (Exception e) {
                        e.printStackTrace();
                        exception = e.getMessage();
                    }
                } else if ((NewStatus != 0) && (NewReloadFulg)) {    //ツイートの読み込み数が0以上で新たに更新するとき
                    try {
                        Paging p = new Paging();
                        p.setSinceId(NewStatus);
                        p.count(200);
                        return mTwitter.getMentionsTimeline(p);
                    } catch (TwitterException e) {
                        e.printStackTrace();
                        exception = e.getMessage();
                    } catch (Exception e) {
                        e.printStackTrace();
                        exception = e.getMessage();
                    }
                } else if (!NewReloadFulg) {    //古いツイートを取得するとき
                    try {
                        Paging p = new Paging();
                        p.setMaxId(OldStatus);
                        p.count(i);
                        return mTwitter.getMentionsTimeline(p);
                    } catch (TwitterException e) {
                        e.printStackTrace();
                        exception = e.getMessage();
                    } catch (Exception e) {
                        e.printStackTrace();
                        exception = e.getMessage();
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(List<twitter4j.Status> result) {
                if (result != null) {
                    position = lv.getFirstVisiblePosition();
                    try {
                        y = lv.getChildAt(0).getTop();
                    } catch (Exception e) {
                        y = 0;
                    }
                    int count = 0;
                    if (NewReloadFulg) {
                        for (twitter4j.Status status : result) {
                            if (count == 0) {
                                MainActivity.MenStatusIDs.add(0, null);
                                count++;
                            }
                            MainActivity.MenStatusIDs.add(count, status);
                            count++;
                        }
                    } else {
                        for (twitter4j.Status status : result) {
                            if (count == 0) {
                                MainActivity.MenStatusIDs.remove(status);
                            }
                            MainActivity.MenStatusIDs.add(status);
                            count++;
                        }
                    }
                    if (count == 0) {
                        MainActivity.MenStatusIDs.add(0,null);
                        MainActivity.showToast("新着なし。");
                    } else {
                        if (OldStatus == 0 || !NewReloadFulg) {
                            MainActivity.MenStatusIDs.add(null);
                        }
                        mAdapter.clear();
                        for (twitter4j.Status tweet : MainActivity.MenStatusIDs) {
                            mAdapter.add(tweet);
                        }
                        MainActivity.showToast(count + "件更新完了！");
                    }
                    if (NewStatus != 0 && NewReloadFulg) {
                        lv.setSelectionFromTop(position + count + 1, y);    //ListViewの表示位置をずらす
                    }
                    try {
                        NewStatus = mAdapter.getItem(1).getId();
                        OldStatus = mAdapter.getItem(mAdapter.getCount() - 2).getId();
                    }catch (Exception e){
                        e.getStackTrace();
                    }
                } else {
                    MainActivity.showDialog("リプライの取得に失敗しました・・・\n" + exception);
                }
                limit.getMentionTimeLineLimit(mTwitter);
                MainActivity.progresStop();
            }
        };
        task.execute();
    }

    @Override
    public void onResume() {
        super.onResume();

        final boolean LongTap = sp.getBoolean("Tap_Setting", true);

        mAdapter.clear();//バグ回避用
        if (MainActivity.MenStatusIDs != null) {
            if (MainActivity.MenStatusIDs.size() != 0) {
                for (Status status : MainActivity.MenStatusIDs) {
                    mAdapter.add(status);
                }
            }
        }

        //ListViewのクリックリスナー登録
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            //通常押し
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //フッターがクリックされた
                if (position != 0 && mAdapter.getItem(position) == null) {
                    NewReloadFulg = false;
                    MainActivity.MenStatusIDs.remove(position);
                    reloadMentionTimeLine();
                }
                //ヘッダーがクリックされた
                if (position == 0 && mAdapter.getItem(position) == null) {
                    MainActivity.MenStatusIDs.remove(0);
                    NewReloadFulg = true;
                    reloadMentionTimeLine();
                }

                if (LongTap == false) {
                    try {
                        Tweet = mAdapter.getItem(position);
                        List_Menu list = new List_Menu(Activity);
                        list.Tweet_Menu(Tweet, mTwitter);
                    } catch (Exception e) {
                        e.printStackTrace();
                        MainActivity.showDialog(e.getMessage());
                    }
                }
            }

        });

        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            //長押し
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if (LongTap == true) {
                    try {
                        Tweet = mAdapter.getItem(position);
                        List_Menu list = new List_Menu(Activity);
                        list.Tweet_Menu(Tweet, mTwitter);
                    } catch (Exception e) {
                        e.printStackTrace();
                        MainActivity.showDialog(e.getMessage());
                    }
                }
                return true;
            }
        });

    }
}
