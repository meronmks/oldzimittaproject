package com.meronmks.zimitta;

import java.util.ArrayList;
import java.util.List;

import twitter4j.PagableResponseList;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.User;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.PaintDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class UserFollowActivity extends ListActivity {

	private FollowAdapter mAdapter;
    private Twitter mTwitter;
    private long UserID = 0;
    private long Next_cursor = 0;
    private String ScreenName;
    private SharedPreferences sp;
    private boolean NewReloadFulg = true;
    private List<User> StatusIDs = new ArrayList<User>();
    private ProgressDialog progressDialog;
    private  SharedPreferences accountIDCount;

    //定数
    public static final String cache = "mention_cache.txt";

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@SuppressLint("NewApi")
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sp = PreferenceManager.getDefaultSharedPreferences(this);
        accountIDCount = getSharedPreferences("accountidcount", 0);
        if (!TwitterUtils.hasAccessToken(this,accountIDCount.getLong("ID_Num_Now", 0))) {
            Intent intent = new Intent(this, TwitterOAuthActivity.class);
            startActivity(intent);
            finish();
        }else {
            mAdapter = new FollowAdapter(this);
            setListAdapter(mAdapter);
            ListView lv = getListView();
            final boolean LongTap = sp.getBoolean("Tap_Setting", true);

    		lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
    			//通常押し
    			@Override
    			public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
    				if(LongTap == false){
    					List_Menu(position);
    				}
    			}
    		});

    		lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
    			//長押し
    			@Override
    			public boolean onItemLongClick(AdapterView<?> parent, View view,int position, long id) {
    				if(LongTap == true){
    					List_Menu(position);
    				}
    				return true;
    			}
    		});
            mTwitter = TwitterUtils.getTwitterInstance(this,accountIDCount.getLong("ID_Num_Now", 0));
            ShowprogressDialog();
            Intent Intent = getIntent();
            UserID = Intent.getLongExtra("UserID_TL", BIND_ABOVE_CLIENT);
            ScreenName = Intent.getStringExtra("ScreenName");
            if(UserID != 0){
            	setTitle(ScreenName + "のフォロー一覧");
            	reloadUserTimeLine();
            }
        }
    }

	//Activity動作の一歩前
    @Override
	protected void onResume() {
		super.onResume();
		PaintDrawable paintDrawable = new PaintDrawable(Color.argb(255,0,0,0));
        getWindow().setBackgroundDrawable(paintDrawable);
	}

	//タイムラインの非同期取得
	private void reloadUserTimeLine() {
	    AsyncTask<Void, Void, PagableResponseList<User>> task = new AsyncTask<Void, Void, PagableResponseList<User>>() {
	    	String exception;
	    	long cursor = -1L;
	    	@Override
	        protected PagableResponseList<User> doInBackground(Void... params) {
		            try {
		            	if(!NewReloadFulg){
		            		cursor = Next_cursor;
		            	}
		                return mTwitter.getFriendsList(UserID, cursor);
					} catch (TwitterException e){
						e.printStackTrace();
						exception = e.getMessage();
					} catch(Exception e)
					{
						e.printStackTrace();
						exception = e.getMessage();
					}
	            return null;
	        }

		        @Override
		        protected void onPostExecute(PagableResponseList<User> result) {
		        	if (result != null) {
		        		for (User status : result) {
		                	StatusIDs.add(status);
		                	mAdapter.add(status);
		                }
		        		Next_cursor = result.getNextCursor();
		        		progressDialog.dismiss();
		        	}else{
		        		showDialog(exception);
		        	}
		        }
		    };
		    task.execute();
		}

		//通信中とかのダイアログをだす
	private void ShowprogressDialog() {
		 progressDialog = new ProgressDialog(UserFollowActivity.this);
         progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
         progressDialog.setCancelable(true);
         progressDialog.setMessage("通信中・・・");
         progressDialog.setCancelable(false);
         progressDialog.show();
	}

	private void List_Menu(final int position){
		String[] dialogItem;
		if(position != StatusIDs.size() - 1){
		dialogItem = new String[]{"ユーザー詳細"};	//メニューの項目作り
		}else{
		dialogItem = new String[]{"ユーザー詳細","さらに読み込む"};	//メニューの項目作り
		}
		AlertDialog.Builder dialogMenu = new AlertDialog.Builder(this);
        dialogMenu.setItems(dialogItem, new DialogInterface.OnClickListener() {
        	@Override
            public void onClick(DialogInterface dialog, int which) {
            	switch(which)
            	{
            	case 0://ユーザー詳細
            		Intent My_Prof_Intent = new Intent(UserFollowActivity.this, Prof_Activiti.class);
            		My_Prof_Intent.putExtra("UserID", StatusIDs.get(position).getId());
    	        	startActivity(My_Prof_Intent);
            		break;
            	case 1: //更新
            		NewReloadFulg = false;
            		ShowprogressDialog();
            		reloadUserTimeLine();
            		break;
            	}
        	}
        }).create().show();
	}

	private void showDialog(String text){
		AlertDialog.Builder alertDialog=new AlertDialog.Builder(this);
		alertDialog.setTitle("Error!");      //タイトル設定
        alertDialog.setMessage(text);  //内容(メッセージ)設定

        // OK(肯定的な)ボタンの設定
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // OKボタン押下時の処理
            }
        });
        alertDialog.show();
	}
}