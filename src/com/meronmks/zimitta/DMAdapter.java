package com.meronmks.zimitta;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.loopj.android.image.SmartImageView;
import twitter4j.DirectMessage;
import twitter4j.MediaEntity;
import twitter4j.URLEntity;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by p-user on 2015/01/20.
 */
public class DMAdapter extends ArrayAdapter<DirectMessage> {
    private LayoutInflater mInflater;
    private String TweetText;

    public DMAdapter(Context context) {
        super(context, android.R.layout.simple_list_item_1);
        mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
    }

    static class ViewHolder {
        SmartImageView icon,rticon;
        TextView name;
        TextView screenName;
        TextView text;
        TextView time;
        TextView rt_To;
        TextView rt;
        TextView fav;
        TextView via;
        ImageView tweetStatus;
        RelativeLayout relativeLayout;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        twitter4j.DirectMessage item = getItem(position);
        if(item != null) {
            ViewHolder holder;
            Date TimeStatusNow = null;
            Date TimeStatusTweet = null;
            Calendar cal1 = Calendar.getInstance();
            Calendar cal2 = Calendar.getInstance();
            Long My_ID = MainActivity.Userid;	//共有変数の呼び出し
            TimeStatusNow = new Date();
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.list_item_tweet, null);

                SmartImageView Icon = (SmartImageView) convertView.findViewById(R.id.icon);	//画像View
                SmartImageView RTIcon = (SmartImageView) convertView.findViewById(R.id.rticon);	//画像View
                TextView Name = (TextView) convertView.findViewById(R.id.name);		//名前View
                TextView ScreenName = (TextView) convertView.findViewById(R.id.screen_name);	//ＩＤView
                TextView Text = (TextView) convertView.findViewById(R.id.text);		//ツイート本文View
                TextView Time =(TextView) convertView.findViewById(R.id.time);	//投稿時間View
                TextView RT_To = (TextView) convertView.findViewById(R.id.RT_to);	//ＲＴした人View
                TextView RT =(TextView) convertView.findViewById(R.id.RT);		//RT数View
                TextView Fav =(TextView) convertView.findViewById(R.id.Fav);	//お気に入り数View
                TextView Via =(TextView) convertView.findViewById(R.id.via);	//投稿されたクライアント名Viwe
                ImageView TweetStatus = (ImageView) convertView.findViewById(R.id.TweetStatus);	//右の帯Viwe
                RelativeLayout RelativeLayout = (RelativeLayout) convertView.findViewById(R.id.Tweet_List);

                holder = new ViewHolder();
                holder.icon = Icon;
                holder.rticon = RTIcon;
                holder.name = Name;
                holder.screenName = ScreenName;
                holder.text = Text;
                holder.time = Time;
                holder.rt_To = RT_To;
                holder.rt = RT;
                holder.fav = Fav;
                holder.via = Via;
                holder.tweetStatus = TweetStatus;
                holder.relativeLayout = RelativeLayout;
                convertView.setTag(holder);

            } else {
                holder = (ViewHolder)convertView.getTag();
            }
            if(holder == null){
                convertView = mInflater.inflate(R.layout.list_item_tweet, null);

                SmartImageView Icon = (SmartImageView) convertView.findViewById(R.id.icon);	//画像View
                SmartImageView RTIcon = (SmartImageView) convertView.findViewById(R.id.rticon);	//画像View
                TextView Name = (TextView) convertView.findViewById(R.id.name);		//名前View
                TextView ScreenName = (TextView) convertView.findViewById(R.id.screen_name);	//ＩＤView
                TextView Text = (TextView) convertView.findViewById(R.id.text);		//ツイート本文View
                TextView Time =(TextView) convertView.findViewById(R.id.time);	//投稿時間View
                TextView RT_To = (TextView) convertView.findViewById(R.id.RT_to);	//ＲＴした人View
                TextView RT =(TextView) convertView.findViewById(R.id.RT);		//RT数View
                TextView Fav =(TextView) convertView.findViewById(R.id.Fav);	//お気に入り数View
                TextView Via =(TextView) convertView.findViewById(R.id.via);	//投稿されたクライアント名Viwe
                ImageView TweetStatus = (ImageView) convertView.findViewById(R.id.TweetStatus);	//右の帯Viwe
                RelativeLayout RelativeLayout = (RelativeLayout) convertView.findViewById(R.id.Tweet_List);

                holder = new ViewHolder();
                holder.icon = Icon;
                holder.rticon = RTIcon;
                holder.name = Name;
                holder.screenName = ScreenName;
                holder.text = Text;
                holder.time = Time;
                holder.rt_To = RT_To;
                holder.rt = RT;
                holder.fav = Fav;
                holder.via = Via;
                holder.tweetStatus = TweetStatus;
                holder.relativeLayout = RelativeLayout;
                convertView.setTag(holder);
            }
            holder.icon.setVisibility(View.VISIBLE);
            holder.name.setVisibility(View.VISIBLE);
            holder.screenName.setVisibility(View.VISIBLE);
            holder.text.setVisibility(View.VISIBLE);
            holder.time.setVisibility(View.VISIBLE);
            holder.rt_To.setVisibility(View.VISIBLE);
            holder.rt.setVisibility(View.GONE);
            holder.fav.setVisibility(View.GONE);
            holder.via.setVisibility(View.GONE);
            holder.tweetStatus.setVisibility(View.GONE);
            holder.icon.setImageUrl(item.getSender().getProfileImageURLHttps());
            holder.name.setText(item.getSender().getName());
            holder.screenName.setText("@" + item.getSender().getScreenName());
            TweetText = item.getText();
            //短縮URL置換
            MediaEntity[] ImgLink = null;
            if (item.getMediaEntities() != null) {
                ImgLink = item.getMediaEntities();
            } else if (item.getExtendedMediaEntities() != null) {
                ImgLink = item.getExtendedMediaEntities();
            }
            if (ImgLink.length != 0) {
                for (int i = 0; i < ImgLink.length; i++) {
                    TweetText = TweetText.replaceAll(ImgLink[i].getURL(), ImgLink[i].getMediaURL());
                }
            }

            URLEntity[] UrlLink = null;
            if (item.getURLEntities() != null) {
                UrlLink = item.getURLEntities();
            } else if (item.getExtendedMediaEntities() != null) {
                UrlLink = item.getURLEntities();
            }
            for (int i = 0; i < UrlLink.length; i++) {
                TweetText = TweetText.replaceAll(UrlLink[i].getURL(), UrlLink[i].getExpandedURL());
            }

            holder.text.setText(TweetText);
            TimeStatusTweet = item.getCreatedAt();
            cal1.setTime(TimeStatusTweet);
            cal2.setTime(TimeStatusNow);
            long date1 = cal1.getTimeInMillis();
            long date2 = cal2.getTimeInMillis();
            long time = (date2 - date1) / 1000;
            if (time <= 59) {
                holder.time.setText(time + "s前");
            }
            time = time / 60;
            if ((time <= 59) && (time >= 1)) {
                holder.time.setText(time + "m前");
            }
            time = time / 60;
            if ((time <= 23) && (time >= 1)) {
                holder.time.setText(time + "h前");
            }
            time = time / 24;
            if (time != 0) {
                holder.time.setText(item.getCreatedAt().toString());
            }
            holder.rt_To.setVisibility(View.GONE);    //RTした人の名前を非表示
            holder.rticon.setVisibility(View.GONE);
            holder.relativeLayout.setBackgroundResource(R.drawable.listitem_color);
        }else{
            convertView = mInflater.inflate(R.layout.list_item_null, null);
        }
        return convertView;
    }


}
