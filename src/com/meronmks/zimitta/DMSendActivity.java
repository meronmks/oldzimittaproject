package com.meronmks.zimitta;

import java.io.InputStream;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.PaintDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("NewApi")
public class DMSendActivity extends Activity {
	private EditText mInputText;
    private Twitter mTwitter;
    public long mentionID;
    public String StatusID;
    private ProgressDialog progressDialog;
    private Uri uri = null;
    private String path = null;
    private TextView textCount;
    private  SharedPreferences accountIDCount;
    //private DirectMessage DMmessege;

	@SuppressLint("NewApi")
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tweet);

        Button Image_button = (Button)this.findViewById(R.id.Image_button);
		Image_button.setText("使用不可");

        Intent intent = getIntent();
        mentionID = intent.getLongExtra("mentionID", BIND_ABOVE_CLIENT);
        StatusID = intent.getStringExtra("StatusID");
        accountIDCount = getSharedPreferences("accountidcount", 0);
        mTwitter = TwitterUtils.getTwitterInstance(this,accountIDCount.getLong("ID_Num_Now", 0));

        //TextView ScreenName = (TextView)this.findViewById(R.id.m_screen_name);	//IDの受け取り
        //ScreenName.setText("@" + StatusID);										//表示
        //TextView Name = (TextView)this.findViewById(R.id.m_name);				//表示名の受け取り
        //Name.setText(name);
          mInputText = (EditText) findViewById(R.id.input_text);					//テキストエディター部分への初期書き込み

          textCount = ((TextView)findViewById(R.id.textCount));

          mInputText.addTextChangedListener(new TextWatcher(){
              @Override
              public void onTextChanged(CharSequence s, int start, int before, int count){
                  int textColor = Color.GRAY;

                  // 入力文字数の表示
                  int txtLength = 140 - s.length();
                  textCount.setText(Integer.toString(txtLength) + "");

                  // 指定文字数オーバーで文字色を赤くする
                  if (txtLength < 0) {
                      textColor = Color.RED;
                  }
                  textCount.setTextColor(textColor);
              }

  			@Override
  			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
  				// TODO 自動生成されたメソッド・スタブ

  			}

  			@Override
  			public void afterTextChanged(Editable s) {
  				// TODO 自動生成されたメソッド・スタブ

  			}
          });

//        mInputText.setText("@" + StatusID + " ");
//        CharSequence str = mInputText.getText();
//        mInputText.setSelection(str.length());
//        TextView Tweet = (TextView)this.findViewById(R.id.m_text);				//ツイート本文の受け取り
//        Tweet.setMovementMethod(ScrollingMovementMethod.getInstance());
//        Tweet.setText(tweet);
//        SmartImageView icon = (SmartImageView) this.findViewById(R.id.m_icon);
//        icon.setImageUrl(Image);
//
        // ビュー
        final View view = this.findViewById(R.id.Activity_Tweet);
        view.setBackgroundColor(Color.BLACK);

        findViewById(R.id.Image_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            	//これでギャラリー画像の選択が可能
            	Intent intent = new Intent(Intent.ACTION_PICK);
        		intent.setType("image/*");
        		startActivityForResult(intent, 1);
            }
        });

        findViewById(R.id.action_tweet).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            	ShowprogressDialog();
                sendDM();
            }
        });


    }

    @Override
	protected void onResume() {
		super.onResume();
		//色バグ対策用
		PaintDrawable paintDrawable = new PaintDrawable(Color.argb(255,0,0,0));
        getWindow().setBackgroundDrawable(paintDrawable);
	}

    //ギャラリーから画像選択（ただし現在使えない）
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    	  super.onActivityResult(requestCode, resultCode, data);
    	  if (resultCode != RESULT_OK) return;

    	  if (requestCode == 1) {
    	    uri= data.getData();
    	    ContentResolver cr = getContentResolver();
	  		String[] columns = { MediaStore.Images.Media.DATA };
	  		Cursor c = cr.query(uri, columns, null, null, null);

	  		c.moveToFirst();
	  		path = c.getString(0);
	  		Button Image_button = (Button)this.findViewById(R.id.Image_button);
			Image_button.setText("");
			try {
				InputStream in = getContentResolver().openInputStream(data.getData());
				Bitmap img = BitmapFactory.decodeStream(in);
				in.close();
				ImageView Select_Image_button = (ImageView) this.findViewById(R.id.Select_Image_button);
				Select_Image_button.setImageBitmap(img);
				Select_Image_button.setAlpha(90);
			}catch (Exception e) {
				e.printStackTrace();
			}
    	}
    }

    //DM送信
    private void sendDM() {
        AsyncTask<String, Void, Boolean> task = new AsyncTask<String, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(String... params) {
                try {
                	if(path != null){
//	                	StatusUpdate update = new StatusUpdate(params[0]);
//	            		UploadedMedia media1 = mTwitter.uploadMedia(new File(path));
//	            		update.setMediaIds(new long[]{media1.getMediaId()});
//	            		update.inReplyToStatusId(mentionID);
//	            		mTwitter.updateStatus(update);
                    	}else{
                    		mTwitter.sendDirectMessage(mentionID, params[0]);
                    	}
                	return true;
				} catch (TwitterException e){
					e.printStackTrace();
					progressDialog.dismiss();
                    e.printStackTrace();
                    return false;
				}catch (Exception e) {
					e.printStackTrace();
                	progressDialog.dismiss();
                    e.printStackTrace();
                    return false;
                }
            }

            @Override
            protected void onPostExecute(Boolean result) {
                if (result) {
                	progressDialog.dismiss();
                    showToast("DM送信完了！");
                    finish();
                } else {
                	progressDialog.dismiss();
                    showToast("DM送信に失敗しました。。。");
                }
            }
        };
        task.execute(mInputText.getText().toString());
    }

    //通信中とかのダイアログをだす
    private void ShowprogressDialog() {
		progressDialog = new ProgressDialog(DMSendActivity.this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(true);
        progressDialog.setMessage("送信中・・・");
        progressDialog.show();
	}

    private void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }
}
