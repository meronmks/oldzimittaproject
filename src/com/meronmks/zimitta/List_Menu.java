package com.meronmks.zimitta;

import twitter4j.*;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipDescription;
import android.text.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.ShareCompat;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class List_Menu {
	static Context context;

	public List_Menu(Context c) {
		this.context = c;
	}

	public void Prof_Menu(final Twitter mTwitter, final User user){
		String[] dialogItem;
		dialogItem = new String[]{"このユーザをブロック"};	//メニューの項目作り
		AlertDialog.Builder dialogMenu = new AlertDialog.Builder(context);
		dialogMenu.setItems(dialogItem, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				switch(which)
				{
					case 0:
						AsyncTask<Void, Void,  Boolean> task = new AsyncTask<Void, Void, Boolean>() {
							//処理をここに書く
							@Override
							protected Boolean doInBackground(Void... params) {
								try{
									mTwitter.createBlock(user.getId());
									return true;
								} catch (TwitterException e){
									e.printStackTrace();
								} catch(Exception e)
								{
									e.printStackTrace();
								}
								return false;
							}
							//処理が終わった後の処理
							@Override
							protected void onPostExecute(Boolean result) {
								if (result != false) {
									MainActivity.showToast("ブロックしました。");
								} else {
									MainActivity.showToast("ブロック失敗・・・");
								}
							}
						};
						task.execute();
						break;
				}
			}
		}).create().show();
	}

	public void App_Menu(final String authenticity_token,final String app_oauth,final AsyncHttpClient client){
		String[] dialogItem;
		dialogItem = new String[]{"連携解除"};	//メニューの項目作り
		AlertDialog.Builder dialogMenu = new AlertDialog.Builder(context);
        dialogMenu.setItems(dialogItem, new DialogInterface.OnClickListener() {
        	@Override
            public void onClick(DialogInterface dialog, int which) {
            	switch(which)
            	{
            	case 0:
            		RequestParams params = new RequestParams();
      				params.put("authenticity_token", authenticity_token);
      				params.put("token", app_oauth);
      		        //連携アプリ一覧取得開始
      				client.post("https://twitter.com/oauth/revoke",params, new AsyncHttpResponseHandler(){ // client.get を client.post にすれば、POST通信もできます
						@Override
						public void onStart(){
							// 通信開始時の処理
						}
						@Override
						public void onSuccess(String response){
							// 通信成功時の処理
							MainActivity.showToast("連携解除しました");
						}

						@Override
						public void onFailure(Throwable e, String response){
							// 通信失敗時の処理
							MainActivity.showToast("解除失敗・・");
						}

						@Override
						public void onFinish(){
							// 通信終了時の処理
						}
					});
            		break;
            	}
        	}
        }).create().show();
	}

	public void UserTimeLine_Menu(final Status Tweet,final Twitter mTwitter){
		String[] dialogItem;
		dialogItem = new String[]{"詳細","会話","返信", "リツイート", "ふぁぼ","リンク先処理","DM送信","共有"};	//メニューの項目作り
		AlertDialog.Builder dialogMenu = new AlertDialog.Builder(context);
        dialogMenu.setItems(dialogItem, new DialogInterface.OnClickListener() {
        	@Override
            public void onClick(DialogInterface dialog, int which) {
            	switch(which)
            	{
            	case 0:	//ツイート詳細
            		Intent intent_Detail = new Intent(context,TweetDetail.class);
            		intent_Detail.putExtra("Detail",Tweet);
            		intent_Detail.putExtra("Twitter",mTwitter);
            		context.startActivity(intent_Detail);
            		break;
            	case 1:	//会話
            		Intent intent_Mention_Line = new Intent(context, MentionToMentionLineActivity.class);
            		intent_Mention_Line.putExtra("StutusID", Tweet.getId());
            		context.startActivity(intent_Mention_Line);
            		break;
            	case 2:		//リプライ
            		Intent intent = new Intent(context, MentionsActivity.class);
            		intent.putExtra("mentionID", Tweet.getId());
            		intent.putExtra("StatusID", Tweet.getUser().getScreenName());
            		intent.putExtra("Name", Tweet.getUser().getName());
            		intent.putExtra("Tweet", Tweet.getText());
            		intent.putExtra("Image", Tweet.getUser().getProfileImageURL());
            		String[] name = new String[20];
            		int j = 0;
            		for (UserMentionEntity UrlLink : Tweet.getUserMentionEntities()) {
            			name[j] = UrlLink.getScreenName();
            			j++;
            		}
            		intent.putExtra("UserMentionEntities", name);
            		context.startActivity(intent);
            		break;
            	case 3:		//リツイート
            		new AlertDialog.Builder(context)
            		.setTitle("RTしてよろしいですか？")
            		.setPositiveButton(
            		"はい",
            		new DialogInterface.OnClickListener() {
            			public void onClick(DialogInterface dialog, int which) {
            				// OK時の処理
            				RTPost(Tweet,mTwitter);
            			}
            		})
            		.setNegativeButton(
            		"いいえ",
            		new DialogInterface.OnClickListener() {
            			public void onClick(DialogInterface dialog, int which) {
            				// NO時の処理
            			}
            		})
            		.show();

            		break;
            	case 4:		//お気に入り

            		new AlertDialog.Builder(context)
            		.setTitle("ふぁぼしますか？")
            		.setPositiveButton(
            		"はい",
            		new DialogInterface.OnClickListener() {
            			public void onClick(DialogInterface dialog, int which) {
            				// OK時の処理
            				FaPost(Tweet,mTwitter);
            			}
            		})
            		.setNegativeButton(
            		"いいえ",
            		new DialogInterface.OnClickListener() {
            			public void onClick(DialogInterface dialog, int which) {
            				// NO時の処理
            			}
            		})
            		.show();
            		break;
            	case 5:	//URLを開く
            		ListMenuAdon list = new ListMenuAdon(context);
            		list.URLList(Tweet);
            		break;
            	case 6:
            		Intent DMintent = new Intent(context, DMSendActivity.class);
            		DMintent.putExtra("StatusID", Tweet.getUser().getScreenName());
            		DMintent.putExtra("mentionID", Tweet.getUser().getId());
            		context.startActivity(DMintent);
    	            break;
            	case 7:	//共有
            		ShareCompat.IntentBuilder builder = ShareCompat.IntentBuilder.from((Activity) context);
            		builder.setText(Tweet.getText());
            		builder.setType("text/plain");
            		// Intent を起動する
                    builder.startChooser();
            		break;
            	default:
					MainActivity.showToast("メニューで例外発生");
            		break;
            	}
        	}
        }).create().show();
	}

	public void Tweet_Menu(final Status Tweet,final Twitter mTwitter)
	{
		String[] dialogItem;
		if(MainActivity.DebugMode){
			dialogItem = new String[]{"詳細","会話","返信", "リツイート", "ふぁぼ","ユーザー詳細","リンク先処理","DM送信","共有","ShowData"};	//メニューの項目作り
		}else {
			dialogItem = new String[]{"詳細", "会話", "返信", "リツイート", "ふぁぼ", "ユーザー詳細", "リンク先処理", "DM送信", "共有"};    //メニューの項目作り
		}
		AlertDialog.Builder dialogMenu = new AlertDialog.Builder(context);
        dialogMenu.setItems(dialogItem, new DialogInterface.OnClickListener() {
        	@Override
            public void onClick(DialogInterface dialog, int which) {
            	switch(which)
            	{
					case 0:	//ツイート詳細
						Intent intent_Detail = new Intent(context,TweetDetail.class);
						intent_Detail.putExtra("Detail",Tweet);
						intent_Detail.putExtra("Twitter",mTwitter);
						context.startActivity(intent_Detail);
						break;
					case 1:	//会話
						Intent intent_Mention_Line = new Intent(context, MentionToMentionLineActivity.class);
						intent_Mention_Line.putExtra("StutusID", Tweet.getId());
						context.startActivity(intent_Mention_Line);
						break;
					case 2:		//リプライ
						Intent intent = new Intent(context, MentionsActivity.class);
						intent.putExtra("mentionID", Tweet.getId());
						intent.putExtra("StatusID", Tweet.getUser().getScreenName());
						intent.putExtra("Name", Tweet.getUser().getName());
						intent.putExtra("Tweet", Tweet.getText());
						intent.putExtra("Image", Tweet.getUser().getProfileImageURL());
						String[] name = new String[20];
						int j = 0;
						for (UserMentionEntity UrlLink : Tweet.getUserMentionEntities()) {
							name[j] = UrlLink.getScreenName();
							j++;
						}
						intent.putExtra("UserMentionEntities", name);
						context.startActivity(intent);
						break;
					case 3:		//リツイート
						new AlertDialog.Builder(context)
						.setTitle("RTしてよろしいですか？")
						.setPositiveButton(
						"はい",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
								// OK時の処理
								RTPost(Tweet,mTwitter);
							}
						})
						.setNegativeButton(
						"いいえ",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
								// NO時の処理
							}
						})
						.show();

						break;
					case 4:		//お気に入り

						new AlertDialog.Builder(context)
						.setTitle("ふぁぼしますか？")
						.setPositiveButton(
						"はい",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
								// OK時の処理
								FaPost(Tweet,mTwitter);
							}
						})
						.setNegativeButton(
						"いいえ",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
								// NO時の処理
							}
						})
						.show();
						break;
					case 5:	//ユーザー詳細
						final Intent My_Prof_Intent = new Intent(context, Prof_Activiti.class);
						if(Tweet.getRetweetedStatus() == null){
							My_Prof_Intent.putExtra("UserID", Tweet.getUser().getId());
							context.startActivity(My_Prof_Intent);
						}else{
							String[] dialogItems;
							dialogItems = new String[]{"@" + Tweet.getRetweetedStatus().getUser().getScreenName(),"@" + Tweet.getUser().getScreenName()};    //メニューの項目作り
							AlertDialog.Builder dialogMenus = new AlertDialog.Builder(context);
							dialogMenus.setItems(dialogItems, new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									switch(which)
									{
										case 0:
											My_Prof_Intent.putExtra("UserID", Tweet.getRetweetedStatus().getUser().getId());
											context.startActivity(My_Prof_Intent);
											break;
										case 1:
											My_Prof_Intent.putExtra("UserID", Tweet.getUser().getId());
											context.startActivity(My_Prof_Intent);
											break;
									}
								}
							}).create().show();
						}
						break;
					case 6:	//URLを開く
						ListMenuAdon list = new ListMenuAdon(context);
						list.URLList(Tweet);
						break;
					case 7:
						Intent DMintent = new Intent(context, DMSendActivity.class);
						DMintent.putExtra("StatusID", Tweet.getUser().getScreenName());
						DMintent.putExtra("mentionID", Tweet.getUser().getId());
						context.startActivity(DMintent);
						break;
					case 8:	//共有
						ShareCompat.IntentBuilder builder = ShareCompat.IntentBuilder.from((Activity) context);
						builder.setText(Tweet.getText());
						builder.setType("text/plain");
						// Intent を起動する
						builder.startChooser();
						break;
					case 9://デバック用
						ShowDebugStatusAll(Tweet);
						break;
            	default:
					MainActivity.showToast("Tweetメニューで例外発生");
            		break;
            	}
			}
        }).create().show();
	}

	public void DM_Menu(final twitter4j.DirectMessage Tweet,final Twitter mTwitter){

	}

	public void Detail_Menu(final Status Tweet,final Twitter mTwitter)
	{
		String[] dialogItem;
		dialogItem = new String[]{"ツイートをコピー","ユーザー詳細","ツイートのリンクをコピー"};	//メニューの項目作り
		AlertDialog.Builder dialogMenu = new AlertDialog.Builder(context);
        dialogMenu.setItems(dialogItem, new DialogInterface.OnClickListener() {
        	@Override
            public void onClick(DialogInterface dialog, int which) {
        		switch(which)
            	{
        		case 0:	//ツイートのコピー
        		{
					String cd = Tweet.getText();

        			//クリップボードにデータを格納
					ClipboardManager cm = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
					cm.setText(cd);
					MainActivity.showToast("ツイートをコピーしました");
        			break;
        		}
        		case 1:	//ユーザ詳細
        			Intent My_Prof_Intent = new Intent(context, Prof_Activiti.class);
            		if(Tweet.getRetweetedStatus() == null){
            			My_Prof_Intent.putExtra("UserID", Tweet.getUser().getId());
            		}else{
            			My_Prof_Intent.putExtra("UserID", Tweet.getRetweetedStatus().getUser().getId());
            		}
            		context.startActivity(My_Prof_Intent);
        			break;
        		case 2:	//リンクコピー
        		{
					String cd = "https://twitter.com/" + Tweet.getUser().getScreenName() + "/status/" + Tweet.getId();

					//クリップボードにデータを格納
					ClipboardManager cm = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
					cm.setText(cd);
					MainActivity.showToast("ツイートへのリンクをコピーしました");
        			break;
        		}
    			default:
					MainActivity.showToast("Detailメニューにて例外発生");
    				break;
            	}
        	}
        }).create().show();
	}

	private void ShowDebugStatusAll(Status Tweet){
		String DebugText = Tweet.toString();
		DebugText = DebugText.replaceAll(",","\n");
		DebugText = DebugText.replaceAll("=","\n");
		showDialog(DebugText);
	}

	//RTの非同期処理
	private void RTPost(final Status Tweet,final Twitter mTwitter)	{
		AsyncTask<Void, Void,  Boolean> task = new AsyncTask<Void, Void, Boolean>() {
			//処理をここに書く
			@Override
	        protected Boolean doInBackground(Void... params) {
				try{
					mTwitter.retweetStatus(Tweet.getId());
					return true;

				} catch (TwitterException e){
					e.printStackTrace();
				} catch(Exception e)
				{
					e.printStackTrace();
				}
				return false;
			}
			//処理が終わった後の処理
			@Override
	        protected void onPostExecute(Boolean result) {
				if (result != false) {
					MainActivity.showToast("リツイートしました。");
	            } else {
					MainActivity.showToast("リツイートに失敗しました。。。");
	            }
			}
		};
		task.execute();
	}

	//お気に入りの非同期処理
	private void FaPost(final Status Tweet,final Twitter mTwitter)	{
		AsyncTask<Void, Void,  Boolean> task = new AsyncTask<Void, Void, Boolean>() {
			//処理をここに書く
			@Override
	        protected Boolean doInBackground(Void... params) {
				try{
					mTwitter.createFavorite(Tweet.getId());
					return true;
				} catch (TwitterException e){
					e.printStackTrace();
				} catch(Exception e)
				{
					e.printStackTrace();
				}
				return false;
			}
			//処理が終わった後の処理
			@Override
	        protected void onPostExecute(Boolean result) {
				if (result != false) {
					MainActivity.showToast("ふぁぼしました。");
	            } else {
					MainActivity.showToast("ふぁぼ失敗・・・");
	            }
			}
		};
		task.execute();
	}

	private void showDialog(String text) {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
		alertDialog.setTitle("StatusAll");      //タイトル設定
		alertDialog.setMessage(text);  //内容(メッセージ)設定

		// OK(肯定的な)ボタンの設定
		alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				// OKボタン押下時の処理
			}
		});
		alertDialog.show();
	}
}
