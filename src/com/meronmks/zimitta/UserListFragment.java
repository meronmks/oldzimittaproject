package com.meronmks.zimitta;

import java.util.List;

import android.widget.*;
import com.actionbarsherlock.app.SherlockListFragment;
import twitter4j.*;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class UserListFragment extends SherlockListFragment {

	private Spinner spinner;
	private ArrayAdapter adapter;
	private Context Activity;
	private  SharedPreferences accountIDCount,ScreanNames,sp;
	private Twitter mTwitter;
	private long[] ListIDs;
	private String[] ListNames;
	private TweetAdapter mylistadapter;
	private ShowRateLimit limit;
    private static long OldStatus;
    private static long NewStatus;
    private boolean NewReloadFulg = true;
    private long ListID;
    private ListView lv;
	private Status Tweet;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		//Fragmentではこのメゾット以外でこれを実行すると落ちる
		lv = getListView();

		mylistadapter = new TweetAdapter(Activity);
		setListAdapter(mylistadapter);

		getUserList();
	}

	@Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {
		Activity = getActivity();
        View v = inflater.inflate(R.layout.user_list_fragment, container, false);

        accountIDCount = Activity.getSharedPreferences("accountidcount", 0);
        sp = PreferenceManager.getDefaultSharedPreferences(Activity);

        mTwitter = TwitterUtils.getTwitterInstance(Activity,accountIDCount.getLong("ID_Num_Now", 0));

        limit = new ShowRateLimit(Activity);

        //スピナーの取得
        spinner = (Spinner)v.findViewById(R.id.spinner1);

        adapter = new ArrayAdapter(Activity, android.R.layout.simple_spinner_item);

        //ボタンの取得
		ImageButton reload = (ImageButton)v.findViewById(R.id.button1);

        reload.setOnClickListener(new View.OnClickListener() {

    	    @Override
    	    public void onClick(View v) {
    	        // クリックの処理を実行する
    	    	mylistadapter.clear();
				MainActivity.myListStatusAdapter.clear();
    	    	NewStatus = 0;
    	    	NewReloadFulg = true;
    	    	ListID = ListIDs[spinner.getSelectedItemPosition()];
    	    	reloadListTimeLine();
    	    }

    	});
        return v;
    }

	//ListTimeLineの取得
	private void reloadListTimeLine() {
        MainActivity.progresRun();
	    AsyncTask<Void, Void, List<twitter4j.Status>> task = new AsyncTask<Void, Void, List<twitter4j.Status>>() {
	    	String exception;
	        @Override
	        protected List<twitter4j.Status> doInBackground(Void... params) {
	        	int i = Integer.parseInt(sp.getString("Load_Tweet", "20"));	//設定をStringで受け取り、intに変換
	        	if ((NewStatus == 0) && (NewReloadFulg)){	//ツイートの読み込み数が0で新たに読み込むとき
	            try {
	            	Paging p = new Paging();
	            	p.count(i);
	                return mTwitter.getUserListStatuses(ListID,p);
				} catch (TwitterException e){
					e.printStackTrace();
					exception = e.getMessage();
				} catch(Exception e)
				{
					e.printStackTrace();
					exception = e.getMessage();
				}
	        	}else if((NewStatus != 0) && (NewReloadFulg)){	//ツイートの読み込み数が0以上で新たに更新するとき
	        		try {
	                	Paging p = new Paging();
	                	p.setSinceId(NewStatus);
	                	p.count(200);
//	                	listposition = getListView().getFirstVisiblePosition();
//	                	listposition_y = getListView().getChildAt(0).getTop();
	                    return mTwitter.getUserListStatuses(ListID,p);
					} catch (TwitterException e){
						e.printStackTrace();
						exception = e.getMessage();
					} catch(Exception e)
					{
						e.printStackTrace();
						exception = e.getMessage();
					}
	        	}else if(!NewReloadFulg){	//古いツイートを取得するとき
	        		try {
	                	Paging p = new Paging();
	                	p.setMaxId(OldStatus);
	                	p.count(i);
	                    return mTwitter.getUserListStatuses(ListID,p);
					} catch (TwitterException e){
						e.printStackTrace();
						exception = e.getMessage();
					} catch(Exception e)
					{
						e.printStackTrace();
						exception = e.getMessage();
					}
	        	}
	            return null;
	        }

		        @Override
		        protected void onPostExecute(List<twitter4j.Status> result) {

					int count = 0;
		        	if (result != null) {
//		            	position = lv.getFirstVisiblePosition();
//		            	try{
//		            		y = lv.getChildAt(0).getTop();
//		            	}catch(Exception e){
//		            		y = 0;
//		            	}
		            	Boolean mute = false;
		            	if(NewReloadFulg)
		            	{
			                for (twitter4j.Status status : result) {
//			                	for(long ID : mutelist)
//			                	{
//			                		if((status.getUser().getId() == ID) && (sp.getBoolean("mute_flag", false)))
//			                		{
//			                			mute = true;
//			                		}
//			                	}
			                	if(!mute)
			                	{
                                    if (count == 0) {
                                        MainActivity.myListStatusAdapter.add(0,null);
                                        count++;
                                    }
				                    MainActivity.myListStatusAdapter.add(count,status);
				                    count++;
			                	}
			                }
		            	}else{
		            		for (twitter4j.Status status : result) {
//			                	for(long ID : mutelist)
//			                	{
//			                		if((status.getUser().getId() == ID) && (sp.getBoolean("mute_flag", false)))
//			                		{
//			                			mute = true;
//			                		}
//			                	}
			                	if(!mute)
			                	{
			            			if(count == 0)
				                    {
                                        MainActivity.myListStatusAdapter.remove(status);
				                    }
			            			count++;
				                    MainActivity.myListStatusAdapter.add(status);
			                	}
		            		}
		            	}
		                if(count == 0){
							MainActivity.myListStatusAdapter.add(0,null);
		                	MainActivity.showToast("新着なし。");
		                }else{
                            if (OldStatus == 0 || !NewReloadFulg) {
                                MainActivity.myListStatusAdapter.add(null);
                            }
                            mylistadapter.clear();
                            for (twitter4j.Status tweet : MainActivity.myListStatusAdapter) {
                                mylistadapter.add(tweet);
                            }
		                	MainActivity.showToast(count + "件更新完了！");
		                }
						if(NewStatus != 0 && NewReloadFulg){
//							lv.setSelectionFromTop(position + count + 1, y);	//ListViewの表示位置をずらす
						}
						NewStatus = mylistadapter.getItem(1).getId();
		                OldStatus = mylistadapter.getItem(mylistadapter.getCount() - 2).getId();
		            } else {
		            	MainActivity.showDialog("ListTLの取得に失敗しました・・・\n" + exception);
		            }
		        	limit.getListTimeLineLimit(mTwitter);
                    MainActivity.progresStop();
		        }
		    };
		    task.execute();
		}

	@Override
	public void onResume() {
		super.onResume();

		final boolean LongTap = sp.getBoolean("Tap_Setting", true);

        mylistadapter.clear();

		//Listが消えた際の再表示
		if(MainActivity.myListStatusAdapter != null){
			if(MainActivity.myListStatusAdapter.size() != 0){
				for (Status status : MainActivity.myListStatusAdapter) {
					mylistadapter.add(status);
				}
			}
		}

		//ListViewのクリックリスナー登録
		lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			//通常押し
			@Override
			public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
				//フッターがクリックされた
                if (position != 0 && mylistadapter.getItem(position) == null) {
                    NewReloadFulg = false;
                    MainActivity.myListStatusAdapter.remove(position);
                    reloadListTimeLine();
                }
				//ヘッダーがクリックされた
                if (position == 0 && mylistadapter.getItem(position) == null) {
                    NewReloadFulg = true;
                    MainActivity.myListStatusAdapter.remove(0);
                    reloadListTimeLine();
                }

                if(LongTap == false){
                    try {
                        Tweet = mylistadapter.getItem(position);
                        List_Menu list = new List_Menu(Activity);
                        list.Tweet_Menu(Tweet, mTwitter);
                    } catch (Exception e) {
                        e.printStackTrace();
                        MainActivity.showDialog(e.getMessage());
                    }
                }
			}
		});

		lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
			//長押し
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,int position, long id) {
                if(LongTap == true){
                    try {
                        Tweet = mylistadapter.getItem(position);
                        List_Menu list = new List_Menu(Activity);
                        list.Tweet_Menu(Tweet, mTwitter);
                    } catch (Exception e) {
                        e.printStackTrace();
                        MainActivity.showDialog(e.getMessage());
                    }
                }
				return true;
			}
		});
	}

	private void getUserList()
	{
		AsyncTask<Void, Void, ResponseList<UserList>> task = new AsyncTask<Void, Void, ResponseList<UserList>>() {

			@Override
			protected ResponseList<UserList> doInBackground(Void... params) {
				try {
					ScreanNames = Activity.getSharedPreferences("ScreanNames", 0);
					long id = accountIDCount.getLong("ID_Num_Now", 0);
					StringBuilder sb = new StringBuilder();
		            sb.append("ScreanName");
		            sb.append(id);
		            String str = new String(sb);
		            String name = ScreanNames.getString(str, "");
					if(MainActivity.ListTimeLineIDs == null) {
						return mTwitter.getUserLists(name);
					}else{
						return MainActivity.ListTimeLineIDs;
					}
				} catch (TwitterException e) {
					e.printStackTrace();
				}
				return null;
			}

	        @Override
	        protected void onPostExecute(ResponseList<UserList> result) {
	        	if(result != null)
	        	{
	        		ListIDs = new long[result.size()];
	        		ListNames = new String[result.size()];
					MainActivity.ListTimeLineIDs = result;
	        		int i = 0;
	        		 for (UserList List : result) {
	        			 adapter.add(List.getName());
	        			 ListNames[i] = List.getName();
	        			 ListIDs[i] = List.getId();
	        			 i++;
	        		 }
        			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        			spinner.setAdapter(adapter);
	        	}
	        }
		};
		task.execute();
	}
}
