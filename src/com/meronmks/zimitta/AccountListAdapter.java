package com.meronmks.zimitta;

import twitter4j.User;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.loopj.android.image.SmartImageView;

public class AccountListAdapter extends ArrayAdapter<User> {
	private LayoutInflater mInflater;

	public AccountListAdapter(Context context) {
        super(context, android.R.layout.simple_list_item_1);
        mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
    }

	@Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.list_acount, null);
        }
        User item = getItem(position);
        TextView name = (TextView) convertView.findViewById(R.id.name);
        name.setText(item.getName());
        TextView screenName = (TextView) convertView.findViewById(R.id.atid);
        screenName.setText("@" + item.getScreenName());
        SmartImageView Icon = (SmartImageView)convertView.findViewById(R.id.icon);
        Icon.setImageUrl(item.getProfileImageURLHttps());
        return convertView;
    }
}
