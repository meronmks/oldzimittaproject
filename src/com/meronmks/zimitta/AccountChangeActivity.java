package com.meronmks.zimitta;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.actionbarsherlock.app.SherlockListActivity;
import twitter4j.Twitter;

public class AccountChangeActivity extends SherlockListActivity {

    private SharedPreferences accountIDCount, ScreanNames;
    private AccountListAdapter adapter;
    private Twitter mTwitter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        adapter = new AccountListAdapter(this);
        setListAdapter(adapter);
        accountIDCount = getSharedPreferences("accountidcount", 0);
        long ID = accountIDCount.getLong("ID_Num", 0);

        for (long i = 0; i < ID; i++) {
//			StringBuilder sb = new StringBuilder();
//            sb.append("ScreanName");
//            sb.append(i);
//            String str = new String(sb);
//			adapter.add(ScreanNames.getString(str, ""));
            getUserItem(i);
        }

        ListView listView = getListView();

        // リストビューのアイテムがクリックされた時に呼び出されるコールバックリスナーを登録します
        //ListViewのクリックリスナー登録
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            //通常押し
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Editor e = accountIDCount.edit();
                e.putLong("ID_Num_Now", position);
                e.commit();
                Intent intent = new Intent(AccountChangeActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }

        });
    }

    @Override
    public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
        com.actionbarsherlock.view.MenuInflater inflater = getSupportMenuInflater();
        inflater.inflate(R.menu.acount_add, menu);
        return true;
    }

    //メニューのアイテムを押したときの判別
    @Override
    public boolean onOptionsItemSelected(com.actionbarsherlock.view.MenuItem item) {
        switch (item.getItemId()) {
            case R.id.acount_Menu:        //アカウント追加画面へ
                Intent intent = new Intent(this, TwitterOAuthActivity.class);
                intent.putExtra("Flag", true);
                startActivity(intent);
                //finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void getUserItem(final long i) {
        AsyncTask<Void, Void, twitter4j.User> task = new AsyncTask<Void, Void, twitter4j.User>() {

            @Override
            protected twitter4j.User doInBackground(Void... params) {
                try {
                    mTwitter = TwitterUtils.getTwitterInstance(AccountChangeActivity.this, i);
                    return mTwitter.verifyCredentials();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(twitter4j.User result) {
                if (result != null) {
                    adapter.add(result);
                }
            }
        };
        task.execute();
    }

}
