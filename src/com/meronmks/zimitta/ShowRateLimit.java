package com.meronmks.zimitta;

import java.util.Map;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import twitter4j.RateLimitStatus;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

public class ShowRateLimit {
	private Context context;

	public ShowRateLimit(Context c) {
		this.context = c;
	}

	//HomeTimeLineの更新Limit取得し表示
	public void getHomeTimeLineLimit(final Twitter mTwitter)
	{
		//非同期
    	AsyncTask<Void, Void, Map<String, RateLimitStatus>> task = new AsyncTask<Void, Void, Map<String, RateLimitStatus>>() {

			@Override
			protected Map<String, RateLimitStatus> doInBackground(Void... params) {
				try {
					return mTwitter.getRateLimitStatus();
				} catch (TwitterException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				};
				return null;
			}

			@Override
	        protected void onPostExecute(Map<String, RateLimitStatus> result) {
				if(result != null)
				{
					for (String endpoint : result.keySet()) {
		            	if(endpoint.equals("/statuses/home_timeline"))
		            	{
			                RateLimitStatus status = result.get(endpoint);
			                showToast("更新規制まで残り" + status.getRemaining() + "回");
//			                System.out.println("Endpoint: " + endpoint);
//			                System.out.println(" Limit: " + status.getLimit());
//			                System.out.println(" Remaining: " + status.getRemaining());
//			                System.out.println(" ResetTimeInSeconds: " + status.getResetTimeInSeconds());
//			                System.out.println(" SecondsUntilReset: " + status.getSecondsUntilReset());
		            	}
		            }
				}
			}

    	};
	    task.execute();
	}

	//Mentionの更新Limit取得し表示
	public void getMentionTimeLineLimit(final Twitter mTwitter)
	{
		//非同期
    	AsyncTask<Void, Void, Map<String, RateLimitStatus>> task = new AsyncTask<Void, Void, Map<String, RateLimitStatus>>() {

			@Override
			protected Map<String, RateLimitStatus> doInBackground(Void... params) {
				try {
					return mTwitter.getRateLimitStatus();
				} catch (TwitterException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				};
				return null;
			}

			@Override
	        protected void onPostExecute(Map<String, RateLimitStatus> result) {
				if(result != null)
				{
					for (String endpoint : result.keySet()) {
		            	if(endpoint.equals("/statuses/mentions_timeline"))
		            	{
			                RateLimitStatus status = result.get(endpoint);
			                showToast("更新規制まで残り" + status.getRemaining() + "回");
		            	}
		            }
				}
			}

    	};
	    task.execute();
	}

	//Listの更新Limit取得し表示
	public void getListTimeLineLimit(final Twitter mTwitter)
	{
		//非同期
    	AsyncTask<Void, Void, Map<String, RateLimitStatus>> task = new AsyncTask<Void, Void, Map<String, RateLimitStatus>>() {

			@Override
			protected Map<String, RateLimitStatus> doInBackground(Void... params) {
				try {
					return mTwitter.getRateLimitStatus();
				} catch (TwitterException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				};
				return null;
			}

			@Override
	        protected void onPostExecute(Map<String, RateLimitStatus> result) {
				if(result != null)
				{
					for (String endpoint : result.keySet()) {
		            	if(endpoint.equals("/lists/statuses"))
		            	{
			                RateLimitStatus status = result.get(endpoint);
			                showToast("更新規制まで残り" + status.getRemaining() + "回");
		            	}
		            }
				}
			}

    	};
	    task.execute();
	}

	public void getDMShowLimit(final Twitter mTwitter){
		//非同期
		AsyncTask<Void, Void, Map<String, RateLimitStatus>> task = new AsyncTask<Void, Void, Map<String, RateLimitStatus>>() {

			@Override
			protected Map<String, RateLimitStatus> doInBackground(Void... params) {
				try {
					return mTwitter.getRateLimitStatus();
				} catch (TwitterException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				};
				return null;
			}

			@Override
			protected void onPostExecute(Map<String, RateLimitStatus> result) {
				if(result != null)
				{
					for (String endpoint : result.keySet()) {
						if(endpoint.equals("/direct_messages"))
						{
							RateLimitStatus status = result.get(endpoint);
							showToast("更新規制まで残り" + status.getRemaining() + "回");
						}
					}
				}
			}

		};
		task.execute();
	}

	public void DebugMode(){

		final Twitter mTwitter;
		SharedPreferences accountIDCount = context.getSharedPreferences("accountidcount", 0);
		mTwitter = TwitterUtils.getTwitterInstance(context, accountIDCount.getLong("ID_Num_Now", 0));
		//非同期
		AsyncTask<Void, Void, Map<String, RateLimitStatus>> task = new AsyncTask<Void, Void, Map<String, RateLimitStatus>>() {

			@Override
			protected Map<String, RateLimitStatus> doInBackground(Void... params) {
				try {
					return mTwitter.getRateLimitStatus();
				} catch (TwitterException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				};
				return null;
			}

			@Override
			protected void onPostExecute(Map<String, RateLimitStatus> result) {
				if(result != null)
				{
					showDialog(result.toString());
				}
			}

		};
		task.execute();
	}

	//トースト表示
	private void showToast(String text) {
	    Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
	}

	private void showDialog(String text) {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
		alertDialog.setTitle("LimitAll");      //タイトル設定
		alertDialog.setMessage(text);  //内容(メッセージ)設定

		// OK(肯定的な)ボタンの設定
		alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				// OKボタン押下時の処理
			}
		});
		alertDialog.show();
	}
}
